﻿using UnityEngine;
using System.Collections;

public class SpawnAI : MonoBehaviour {

	//public 
	public Vector2 SpawnArrea;
	public float RespawningTime;
	public GameObject Enemy;
	public GameObject Hero;

	//private
	float RespawnTimer = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!MenuScript.IsPaused)
        {
            RespawnTimer += Time.deltaTime;

            if (RespawnTimer >= RespawningTime)
            {
                Vector3 randomPos = new Vector3(Random.Range(0, SpawnArrea.x) - 8, 0.5f, Random.Range(0, SpawnArrea.y) - 8);
                int SideChooser = Random.Range(0, 10);
                if (randomPos.x < randomPos.z)
                {
                    if (SideChooser < 5)
                        randomPos.x = -8;
                    else
                        randomPos.x = SpawnArrea.x - 8;
                }
                else
                {
                    if (SideChooser < 5)
                        randomPos.z = -8;
                    else
                        randomPos.z = SpawnArrea.y - 8;
                }

                GameObject tempEnemy = GameObject.Instantiate(Enemy, randomPos, Quaternion.identity) as GameObject;
                tempEnemy.transform.LookAt(Hero.transform.position);
                RespawnTimer = 0;
            }
        }
	}
}
