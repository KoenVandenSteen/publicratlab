﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	//public

	//sound effects and music:
	//private
	AudioSource[] _audioSources; 

	// Use this for initialization
	void Start () {

		//read in all the audio sources
		_audioSources = GetComponents<AudioSource> ();


	}

	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayFirstBlood(){
		_audioSources[0].Play ();
	}

	public void PlayMegaKill(){
		_audioSources[1].Play ();
	}

	public void PlayMonsterKill(){
		_audioSources[2].Play ();
	}

	public void PlayEnemyHit(){
		int rand = Random.Range (0, 2);
		if (rand == 0)
			_audioSources [3].Play ();
		else
			_audioSources [4].Play ();
	}

}
