﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class AchievementScript : MonoBehaviour
{
    public enum TRACKABLE_VALUES
    {
        SCORE_LEVEL,
        SCORE_LEVEL_PERCENTAGE,
        UNLOCK_LEVEL,
        UNLOCK_ACHIEVEMENT,
        KILLS_TOTAL,
        KILLS_CURRENT,
        DEATHS_CURRENT,
        DEATHS_TOTAL,
        TIME_LEVEL,
        TIME_PLAYED
    }

    public enum PARAM2
    {
        NULL = 0,
        SMALLER = -1,
        BIGGER = -2,
        DEATH = -3,
        COMPLETE = -4,
        DEATH_SMALLER = -5,
        DEATH_BIGGER = -6,
        COMPLETE_SMALLER = -7,
        COMPLETE_BIGGER = -8
    }
    
    public enum ACHIEVEMENTS
    { 
        CHEATER,        // Complete the level within five seconds
        PERFECTIONIST,  // Complete a level with 100%
        EASY_PREY,      // Die within five seconds of the start of a level
        KILLER_MASTER   // Kill 500 enemies in total
    }

    private struct Achievement
    {
        public bool Completed;
        public string Name;
        public string Description;
        public string Requirements;
        public TRACKABLE_VALUES ReqName;
        public float ReqParam1;
        public PARAM2 ReqParam2;
        public void SetProperties(string n, string d, string r, TRACKABLE_VALUES t, float v1, PARAM2 v2 = PARAM2.SMALLER)
        { Name = n; Description = d; Requirements = r; ReqName = t; ReqParam1 = v1; ReqParam2 = v2; }
    }

    public static int[] _scoreLevel = new int[6];
    private static Achievement[] _Achievements =  new Achievement[5];
    private static float[] _values = new float[10];

	public static void Initialize()
    {
        if (!PlayerPrefs.HasKey(TRACKABLE_VALUES.KILLS_TOTAL.ToString())) PlayerPrefs.SetFloat(TRACKABLE_VALUES.KILLS_TOTAL.ToString(), 0);
        //if (!PlayerPrefs.HasKey(TRACKABLE_VALUES.KILLS_CURRENT.ToString())) PlayerPrefs.SetFloat(TRACKABLE_VALUES.KILLS_CURRENT.ToString(), 0);
        if (!PlayerPrefs.HasKey(TRACKABLE_VALUES.DEATHS_TOTAL.ToString())) PlayerPrefs.SetFloat(TRACKABLE_VALUES.DEATHS_TOTAL.ToString(), 0);
        //if (!PlayerPrefs.HasKey(TRACKABLE_VALUES.DEATHS_CURRENT.ToString())) PlayerPrefs.SetFloat(TRACKABLE_VALUES.DEATHS_CURRENT.ToString(), 0);
        if (!PlayerPrefs.HasKey(TRACKABLE_VALUES.TIME_PLAYED.ToString())) PlayerPrefs.SetFloat(TRACKABLE_VALUES.TIME_PLAYED.ToString(), 0);
        //if (!PlayerPrefs.HasKey(TRACKABLE_VALUES.TIME_LEVEL.ToString())) PlayerPrefs.SetFloat(TRACKABLE_VALUES.TIME_LEVEL.ToString(), 0);
        if (!PlayerPrefs.HasKey(TRACKABLE_VALUES.UNLOCK_ACHIEVEMENT.ToString())) PlayerPrefs.SetString(TRACKABLE_VALUES.UNLOCK_ACHIEVEMENT.ToString(), "");
        if (!PlayerPrefs.HasKey(TRACKABLE_VALUES.UNLOCK_LEVEL.ToString())) PlayerPrefs.SetFloat(TRACKABLE_VALUES.UNLOCK_LEVEL.ToString(), 1);
        if (!PlayerPrefs.HasKey(TRACKABLE_VALUES.SCORE_LEVEL.ToString())) PlayerPrefs.SetString(TRACKABLE_VALUES.SCORE_LEVEL.ToString(), "");
        //if (!PlayerPrefs.HasKey(TRACKABLE_VALUES.SCORE_LEVEL_PERCENTAGE.ToString())) PlayerPrefs.SetFloat(TRACKABLE_VALUES.SCORE_LEVEL_PERCENTAGE.ToString(), 0);

        //_Achievements = new Achievement[4];
        //_values = new float[10];

        _Achievements[0].SetProperties("Cheater",       "Are you happy now ?",     "Complete a level over 100%." ,TRACKABLE_VALUES.SCORE_LEVEL_PERCENTAGE, 100, PARAM2.BIGGER);
        _Achievements[1].SetProperties("Perfectionist", "It's a work of art.",     "Get a score of 100%." ,TRACKABLE_VALUES.SCORE_LEVEL_PERCENTAGE, 100, PARAM2.COMPLETE);
        _Achievements[2].SetProperties("Easy Prey",     "Hmmm, delicious.",        "Die within five seconds." ,TRACKABLE_VALUES.TIME_LEVEL, 5, PARAM2.DEATH_SMALLER);
        _Achievements[3].SetProperties("Killer Master", "You sure showed them.",   "Kill 500 enemies." ,TRACKABLE_VALUES.KILLS_TOTAL, 500, PARAM2.BIGGER);
        _Achievements[4].SetProperties("First Kill",    "They better watch out !", "Kill one enemy." ,TRACKABLE_VALUES.KILLS_CURRENT, 1, PARAM2.NULL);

        LoadAchievements();
	}

    private static int _update = 0; // Using this to only check achievements every ten frames
    public static void Update()
    {
        if (_update >= 10)
        {
            _update = 0;
            CheckAchievements(PARAM2.NULL);
        }
        else ++_update;
    }

    public static void SetValue(TRACKABLE_VALUES v, float val) { _values[(int)v] = val; }
    public static float GetValue(TRACKABLE_VALUES v) { return _values[(int)v]; }

    public static void CheckAchievements(PARAM2 p)
    {
        for (int i = 0; i < _Achievements.Length; i++)
            CheckAchievement(i, p, true);
    }
	
    private static void CheckAchievement(int a, PARAM2 p, bool ShowMessage = true)  
    {
        if (!_Achievements[a].Completed)
        {
            switch (p)
            {
                case PARAM2.NULL:
                    switch (_Achievements[a].ReqParam2)
                    {
                        case PARAM2.SMALLER:
                            if (_values[(int)_Achievements[a].ReqName] < _Achievements[a].ReqParam1)
                            {
                                _Achievements[a].Completed = true;
                                if (ShowMessage) ShowAchievementMessage(a);
                            }
                            break;
                        case PARAM2.BIGGER:
                            if (_values[(int)_Achievements[a].ReqName] > _Achievements[a].ReqParam1)
                            {
                                _Achievements[a].Completed = true;
                                if (ShowMessage) ShowAchievementMessage(a);
                            }
                            break;
                        case PARAM2.NULL:
                            if (_values[(int)_Achievements[a].ReqName] == _Achievements[a].ReqParam1)
                            {
                                _Achievements[a].Completed = true;
                                if (ShowMessage) ShowAchievementMessage(a);
                            }
                            break;
                    }
                    break;
                case PARAM2.DEATH:
                    switch (_Achievements[a].ReqParam2)
                    {
                        case PARAM2.DEATH_SMALLER:
                            if (_values[(int)_Achievements[a].ReqName] < _Achievements[a].ReqParam1)
                            {
                                _Achievements[a].Completed = true;
                                if (ShowMessage) ShowAchievementMessage(a);
                            }
                            break;
                        case PARAM2.DEATH_BIGGER:
                            if (_values[(int)_Achievements[a].ReqName] > _Achievements[a].ReqParam1)
                            {
                                _Achievements[a].Completed = true;
                                if (ShowMessage) ShowAchievementMessage(a);
                            }
                            break;
                        case PARAM2.DEATH:
                            if (_values[(int)_Achievements[a].ReqName] == _Achievements[a].ReqParam1)
                            {
                                _Achievements[a].Completed = true;
                                if (ShowMessage) ShowAchievementMessage(a);
                            }
                            break;
                    }
                    break;
                case PARAM2.COMPLETE:
                    switch (_Achievements[a].ReqParam2)
                    {
                        case PARAM2.COMPLETE_SMALLER:
                            if (_values[(int)_Achievements[a].ReqName] < _Achievements[a].ReqParam1)
                            {
                                _Achievements[a].Completed = true;
                                if (ShowMessage) ShowAchievementMessage(a);
                            }
                            break;
                        case PARAM2.COMPLETE_BIGGER:
                            if (_values[(int)_Achievements[a].ReqName] > _Achievements[a].ReqParam1)
                            {
                                _Achievements[a].Completed = true;
                                if (ShowMessage) ShowAchievementMessage(a);
                            }
                            break;
                        case PARAM2.COMPLETE:
                            if (_values[(int)_Achievements[a].ReqName] == _Achievements[a].ReqParam1)
                            {
                                _Achievements[a].Completed = true;
                                if (ShowMessage) ShowAchievementMessage(a);
                            }
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private static Object _achievementPrefab;
    private static GameObject _canvas;
    private static void ShowAchievementMessage(int a)
    {
        if (_achievementPrefab == null) _achievementPrefab = Resources.Load("AchievementPopUp");
        if (_canvas == null) _canvas = GameObject.FindGameObjectWithTag("UICanvas");

        GameObject message = Instantiate(_achievementPrefab) as GameObject;
        message.transform.SetParent(_canvas.transform, false);
        message.GetComponent<AchievementPopUp>().SetText(_Achievements[a].Name, _Achievements[a].Description, _Achievements[a].Requirements);
    }

    public static void LoadAchievements()
    {
        char[] AchievesGot = PlayerPrefs.GetString(TRACKABLE_VALUES.UNLOCK_ACHIEVEMENT.ToString()).ToCharArray();
        for (int i = 0; i < AchievesGot.Length; i += 2)
            _Achievements[i / 2].Completed = (AchievesGot[i] == '1') ? true : false;

        char[] Scores = PlayerPrefs.GetString(TRACKABLE_VALUES.SCORE_LEVEL.ToString()).ToCharArray();
        for (int i = 0; i < Scores.Length; i += 5)
        {
            for (int j = 0; j < 4; j++)
            {
                string score = ""; 
                score += Scores[i + j];
                _scoreLevel[i / 5] = int.Parse(score);
            }
        }

        _values[(int)TRACKABLE_VALUES.UNLOCK_LEVEL] = PlayerPrefs.GetFloat(TRACKABLE_VALUES.UNLOCK_LEVEL.ToString());
        _values[(int)TRACKABLE_VALUES.KILLS_TOTAL] = PlayerPrefs.GetFloat(TRACKABLE_VALUES.KILLS_TOTAL.ToString());
        _values[(int)TRACKABLE_VALUES.DEATHS_TOTAL] = PlayerPrefs.GetFloat(TRACKABLE_VALUES.KILLS_TOTAL.ToString());
        _values[(int)TRACKABLE_VALUES.TIME_PLAYED] = PlayerPrefs.GetFloat(TRACKABLE_VALUES.KILLS_TOTAL.ToString());
    }

    public static void SaveAllValues() 
    {
        string AchievesGot = "";
        for (int i = 0; i < _Achievements.Length; i++)
            AchievesGot += ((_Achievements[i].Completed) ? "1" : "0") + ",";
        PlayerPrefs.SetString(TRACKABLE_VALUES.UNLOCK_ACHIEVEMENT.ToString(), AchievesGot);

        AchievesGot = "";
        for (int i = 0; i < _scoreLevel.Length; i++)
        {
            if (_scoreLevel[i] > 9999) _scoreLevel[i] = 9999;
            AchievesGot += _scoreLevel[i].ToString("0000") + ",";
        }
        PlayerPrefs.SetString(TRACKABLE_VALUES.SCORE_LEVEL.ToString(), AchievesGot);

        PlayerPrefs.SetFloat(TRACKABLE_VALUES.KILLS_TOTAL.ToString(), _values[(int)TRACKABLE_VALUES.KILLS_TOTAL]);
        PlayerPrefs.SetFloat(TRACKABLE_VALUES.DEATHS_TOTAL.ToString(), _values[(int)TRACKABLE_VALUES.DEATHS_TOTAL]);
        PlayerPrefs.SetFloat(TRACKABLE_VALUES.TIME_PLAYED.ToString(), _values[(int)TRACKABLE_VALUES.TIME_PLAYED]);
        PlayerPrefs.SetFloat(TRACKABLE_VALUES.UNLOCK_LEVEL.ToString(), _values[(int)TRACKABLE_VALUES.UNLOCK_LEVEL]);

        PlayerPrefs.Save(); 
    }
}
