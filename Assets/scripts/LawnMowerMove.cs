﻿using UnityEngine;
using System.Collections;

public class LawnMowerMove : MonoBehaviour 
{
    // Public
	public GameObject GameManager;
	public float MaxMoveSpeed;
	public float Acceleration;
	public float RotateSpeed;
    public float BumpPushBackForce = 10;
	public float RotAngleLimit;
	public float MovementPenalty;

    // Private
	private float _curRotationSpeed = 0;
	private CharacterController _controller;
    private LevelScript _gameScript;
	private float _curMovementSpeed;
	private GameObject _managerScript;
    private static Object _bloodSplatter;

    private Vector3 _vecForward;

	// Use this for initialization
	void Start () 
    {
		_controller = GetComponent<CharacterController>();
        _gameScript = Camera.main.GetComponent<LevelScript>(); 
        if (_bloodSplatter == null) _bloodSplatter = Resources.Load("BloodEffect");
        _vecForward = transform.forward;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!MenuScript.IsPaused)
        {
            // Read input
            float controllerHorizontal = Input.GetAxis("Horizontal"); 

            if (controllerHorizontal < 0)
            {
                transform.Rotate(Vector3.up * -RotateSpeed * Time.deltaTime);
                _vecForward = Quaternion.AngleAxis(-RotateSpeed * Time.deltaTime, Vector3.up) * _vecForward;
            }
            else if (controllerHorizontal > 0)
            {
                transform.Rotate(Vector3.up * RotateSpeed * Time.deltaTime);
                _vecForward = Quaternion.AngleAxis(RotateSpeed * Time.deltaTime, Vector3.up) * _vecForward;
            }

            _controller.SimpleMove(_vecForward.normalized * MaxMoveSpeed);
        }
	}

    public void ResetVelocity()
    {
        _vecForward = transform.forward;
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.collider.tag.Equals("Collide Bump"))
        { }
        else _gameScript.CharacterHitTile(ref _controller, hit);


    }

	void OnTriggerEnter(Collider other){

		if (other.gameObject.tag == "Enemy") {
			if(_curMovementSpeed > 0)
				_curMovementSpeed -= MovementPenalty;
			GameManager.GetComponent<SoundManager>().PlayEnemyHit();
			GameManager.GetComponent<GameManagerScript>().KillCounter++;
			GameManager.GetComponent<GameManagerScript>().CheckKillCounter();
            GameObject.Instantiate(_bloodSplatter, other.transform.position, Quaternion.identity);
			Destroy(other.gameObject);
            AchievementScript.SetValue(AchievementScript.TRACKABLE_VALUES.KILLS_CURRENT, AchievementScript.GetValue(AchievementScript.TRACKABLE_VALUES.KILLS_CURRENT) + 1);
            AchievementScript.SetValue(AchievementScript.TRACKABLE_VALUES.KILLS_TOTAL, AchievementScript.GetValue(AchievementScript.TRACKABLE_VALUES.KILLS_TOTAL) + 1);
            _gameScript.AddScorePopUp(5, other.transform.position);
		}
		
	}

    // ChacterController interferes with rigidbody
    //void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.collider.tag.Equals("Collide Bump"))
    //    {
    //        gameObject.rigidbody.AddExplosionForce(BumpPushBackForce, collision.transform.position, 5);
    //        Debug.Log("BAM");
    //    }
    //}
}
