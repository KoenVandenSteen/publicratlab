﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelScript : MonoBehaviour 
{
    public int MinimumComplete = 70;
    public int LevelNumber = 0;
    public GameObject GrassCutTile, GrassNotCutTile, StoneTile;
    public Text UIGamePercentage, UIGameTime, UIGameScore, UIGameLives;
    public int Lives = 3;

    private float _heightDistance, _widthDistance;
    private int _score = 0;
    private int _tilesToCut = 0, _tilesCut;
    private float _timeLevel = 0, _timeTotal;
    private static Vector3 _startPos;
    public Object _popUpText;

    public static void SetStartPos(Vector3 pos) { _startPos = pos; }

    public Canvas MenuPause;
    private Object _menuGameOver;

	// Use this for initialization
	void Start () 
    {
        if (!MenuScript.Initialized()) { AchievementScript.Initialize(); MenuScript.Initialize(); }
        if (!_menuGameOver) _menuGameOver = Resources.Load("MenuGameOver");
        //if (_popUpText == null) Resources.Load("PointsPopUp");
        LoadLevel(LevelNumber);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!MenuScript.IsPaused)
        {
            float percent = _tilesCut * 100 / _tilesToCut;
            _timeLevel += Time.deltaTime;
            UIGamePercentage.text = "Completed : " + percent + "%";
            UIGameTime.text = "Time : " + Helpers.ToMMSS(_timeTotal - _timeLevel);
            UIGameScore.text = "Score : " + _score;
            UIGameLives.text = "Lives : " + Lives;

            if (_tilesToCut == _tilesCut)
            {
                GameObject menu = Instantiate(_menuGameOver) as GameObject;
                MenuScript.IsPaused = true;
                AchievementScript._scoreLevel[LevelNumber] = _score;
            }
            else if (_timeLevel > _timeTotal)
            { 
                // Time up
                // Continue if score is larger than MinimumScore
                if (percent > MinimumComplete)
                {
                    GameObject menu = Instantiate(_menuGameOver) as GameObject;
                    AchievementScript._scoreLevel[LevelNumber] = _score;
                    MenuScript.IsPaused = true;
                }
                else
                {
                    GameObject menu = Instantiate(_menuGameOver) as GameObject;
                    MenuScript.IsPaused = true;
                }
            }
            else if (Lives == 0)
            {
                GameObject menu = Instantiate(_menuGameOver) as GameObject;
                MenuScript.IsPaused = true;
            }

            AchievementScript.SetValue(AchievementScript.TRACKABLE_VALUES.TIME_LEVEL, _timeLevel);
            AchievementScript.SetValue(AchievementScript.TRACKABLE_VALUES.TIME_PLAYED, AchievementScript.GetValue(AchievementScript.TRACKABLE_VALUES.TIME_PLAYED) + Time.deltaTime);
            AchievementScript.SetValue(AchievementScript.TRACKABLE_VALUES.SCORE_LEVEL_PERCENTAGE, _tilesCut * 100 / _tilesToCut);
            AchievementScript.Update();

            if (Input.GetAxis("Pause") > 0)
            {
                MenuScript.IsPaused = true;
                MenuPause.enabled = true;
            }
        }
	}

    public void DisablePause() { MenuPause.enabled = false; }

    public void DecreaseLives() { --Lives; }

    public void CharacterHitTile(ref CharacterController sender, ControllerColliderHit hit)
    {
        if (hit.collider.tag.Equals("GrassNotCutTile"))
        {
            Instantiate(GrassCutTile, hit.gameObject.transform.position, hit.gameObject.transform.rotation);
            GameObject.Destroy(hit.gameObject);
            AddScorePopUp(10, hit.gameObject.transform.position);
            //Debug.Log("Cut some grass !");
            ++_tilesCut;
        }
    }

    public void Respawn() 
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var item in enemies)
            GameObject.Destroy(item);
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.transform.position = _startPos;
        player.transform.rotation = new Quaternion();
        player.GetComponent<LawnMowerMove>().ResetVelocity();
        DecreaseLives();
    }

    public void AddScorePopUp(int score, Vector3 pos) 
    {
        //GameObject quadText = Instantiate(_popUpText, pos, new Quaternion()) as GameObject;
        //quadText.GetComponent<PopUpScore>().SetText(score.ToString());
        _score += score;
    }

    // Level Loader
    void LoadLevel(int LevelNumber)
    {
        _timeTotal = LevelData.Levels[LevelNumber].Time;

        GameObject[] levelBorders = GameObject.FindGameObjectsWithTag("LevelEdge");
        _heightDistance = Mathf.Abs(levelBorders[0].transform.position.z - levelBorders[1].transform.position.z) / LevelData.Levels[LevelNumber].Height;
        _widthDistance = Mathf.Abs(levelBorders[0].transform.position.x - levelBorders[1].transform.position.x) / LevelData.Levels[LevelNumber].Width;

        Vector3 startPos = new Vector3();
        foreach (var item in levelBorders)
            if (item.name == "LevelEdge1")
            {
                startPos = item.transform.position + new Vector3(_widthDistance / 2, 0, -(_heightDistance / 2));
                startPos.y = 0;
                break;
            }

        Quaternion rotation90Deg = Quaternion.AxisAngle(new Vector3(1, 0, 0), Mathf.PI / 2);

        Vector3 scale = new Vector3(_widthDistance, _heightDistance, 1);
        GrassNotCutTile.transform.localScale = scale;
        GrassCutTile.transform.localScale = scale;

        for (int h = 0; h < LevelData.Levels[LevelNumber].Height; h++)
        {
            for (int w = 0; w < LevelData.Levels[LevelNumber].Width; w++)
            {
                Vector3 tempPos = new Vector3(_widthDistance * w, 0, -(_heightDistance * h));
                switch (LevelData.Levels[LevelNumber].Data[h, w])
                {
                    case 0: // Nothing
                        break;
                    case 1: // Stone tile
                        Instantiate(StoneTile, startPos + tempPos, rotation90Deg);
                        break;
                    case 2: // Cut grass tile
                        Instantiate(GrassCutTile, startPos + tempPos, rotation90Deg);
                        break;
                    case 3: // Uncut grass tile
                        Instantiate(GrassNotCutTile, startPos + tempPos, rotation90Deg);
                        ++_tilesToCut;
                        break;
                    default:
                        break;
                }
                //Debug.Log("created " + LevelData.Levels[LevelNumber].Data[h, w].ToString());
            }
        }
    }
}
