﻿using UnityEngine;
using System.Collections;

public class PopUpScore : MonoBehaviour 
{
    public float Duration = 1;
    public float Speed = 10;
    public TextMesh Text;

    private float _timer;

    public void SetText(string text) { Text.text = text; }

    void Start() 
    {
        //Text = GetComponent<TextMesh>();
    }

    void Update()
    {
        _timer += Time.deltaTime;
        if (_timer >= Duration) Destroy(this.gameObject);
        transform.Translate(Vector3.up * Speed * Time.deltaTime);
    }
}
