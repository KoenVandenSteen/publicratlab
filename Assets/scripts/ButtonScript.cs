﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour 
{
    public Text DescriptiveText, ButtonText;
    public Button Button;

	// Use this for initialization
	void Start () 
    {
        MenuScript.Initialize();
        int level = int.Parse(ButtonText.text);
        int status = (int)AchievementScript.GetValue(AchievementScript.TRACKABLE_VALUES.UNLOCK_LEVEL);
        if (level <= status)
        {
            Button.interactable = true;
            DescriptiveText.text = "Best : " + AchievementScript._scoreLevel[int.Parse(ButtonText.text) - 1].ToString();
        }
        else DescriptiveText.text = "";
	}
}
