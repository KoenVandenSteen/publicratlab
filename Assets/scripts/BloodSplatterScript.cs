﻿using UnityEngine;
using System.Collections;

public class BloodSplatterScript : MonoBehaviour {

    public float Duration = 1;
    private float _timer;

	void Update () 
    {
        _timer += Time.deltaTime;

        if (_timer >= Duration) Destroy(this.gameObject);
	}
}
