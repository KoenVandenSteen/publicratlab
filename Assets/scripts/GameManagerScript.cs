﻿using UnityEngine;
using System.Collections;

public class GameManagerScript : MonoBehaviour {

	//public
	public int KillCounter = 0;

	//private
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	}

	public void CheckKillCounter(){

		switch (KillCounter) {

		case 1: 
			gameObject.GetComponent<SoundManager>().PlayFirstBlood();
			break;
		case 5: 
			gameObject.GetComponent<SoundManager>().PlayMegaKill();
			break;
		case 10: 
			gameObject.GetComponent<SoundManager>().PlayMonsterKill();
			break;
		default:
			break;
		}

	}
}
