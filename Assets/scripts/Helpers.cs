﻿using UnityEngine;
using System.Collections;

public abstract class Helpers : MonoBehaviour 
{
    public static string ToMMSS(float time) 
    {
        string minutes = Mathf.Floor(time / 60).ToString("00");
        string seconds = (time % 60).ToString("00");
        return minutes + ":" + seconds;
    }
}
