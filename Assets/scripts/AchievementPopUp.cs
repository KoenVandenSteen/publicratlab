﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AchievementPopUp : MonoBehaviour 
{
    public Text AchievementTitle, AchievementDescription, AchievementReq;
    public Image AchievementBack;

    public float Speed = 10;
    public float Movement = 110; // px
    private float _movement;
    private bool _moveDown = true;
    private float _timeOut = 3;

	// Use this for initialization
	void Start () 
    {
        _movement = Movement;
        Vector3 newPos = AchievementBack.transform.position;
        newPos.y = Screen.height + 100;
        AchievementBack.transform.position = newPos;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (_moveDown)
        {
            float displ = Speed * Time.deltaTime;
            _movement -= displ;
            if (_movement < 0) { displ += _movement; _moveDown = false; }
            Vector3 newPos = AchievementBack.transform.position;
            newPos.y -= displ;
            AchievementBack.transform.position = newPos;
        }
        else if (_timeOut > 0) { _timeOut -= Time.deltaTime; }
        else
        {
            float displ = Speed * Time.deltaTime;
            _movement += displ;
            Vector3 newPos = AchievementBack.transform.position;
            newPos.y += displ;
            AchievementBack.transform.position = newPos;

            if (_movement > Movement)
            {
                Destroy(AchievementBack);
                foreach (Transform child in transform)
                    GameObject.Destroy(child.gameObject);
                Destroy(this);
            }
        }
	}

    public void SetText(string title, string descr, string descr2)
    {
        AchievementTitle.text = title;
        AchievementDescription.text = descr;
        AchievementReq.text = descr2;
    }
}
