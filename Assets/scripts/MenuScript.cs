﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{
    public Canvas Main, Controls, Level, Achievements;
    private string _oldCanvas = "CanvasMainMenu";

    public void LoadLevel(int i)
    { 
        Application.LoadLevel(i);
        IsPaused = false;
        AchievementScript.SaveAllValues();
    }

    public void SetCanvasInvisible(string name) { SetCanvasVisibility(name, false); }
    public void SetCanvasVisibility(string name, bool visible)
    {
        switch (name)
        {
            case "CanvasMainMenu": Main.enabled = visible; break;
            case "CanvasControls": Controls.enabled = visible; break;
            case "CanvasLevelSelect": Level.enabled = visible; break;
            case "CanvasAchievements": Achievements.enabled = visible; break;
        }    
    }

    public void SwitchCanvas(string name)
    {
        SetCanvasVisibility(_oldCanvas, false);
        SetCanvasVisibility(name, true);
        _oldCanvas = name;
    }

    private static bool _initDone = false;
    public static bool Initialized() { return _initDone; }
    void Start()
    {
        Initialize();
    }

    public static void Initialize()
    {
        if (!_initDone)
        {
            AchievementScript.Initialize();
            LevelData.BuildLevels();
            _initDone = true;
        }
    }

    public static bool IsPaused;
    public void SetPaused(bool isPaused) { IsPaused = isPaused; }

    public void UnPause()
    {
        GetComponent<LevelScript>().MenuPause.enabled = false;
        IsPaused = false;
    }

    public void CheckLevelEnabledButtons()
    {
        //GameObject[] buttons = GameObject.FindGameObjectsWithTag("ButtonLevelSelect");
        
    }
}
