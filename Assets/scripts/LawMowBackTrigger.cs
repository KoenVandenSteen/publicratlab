﻿using UnityEngine;
using System.Collections;

public class LawMowBackTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		
		if (other.gameObject.tag == "Enemy") {
            AchievementScript.CheckAchievements(AchievementScript.PARAM2.DEATH);
            Camera.main.GetComponent<LevelScript>().Respawn();
		}
		
	}
}
