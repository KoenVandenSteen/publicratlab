﻿using UnityEngine;
using System.Collections;

public class AI_Base : MonoBehaviour {


	//public
	public GameObject Hero;
	public float MoveSpeed;
	public float RotationSpeed;
	// Use this for initialization
	void Start () {
		Hero = GameObject.Find ("lawnmower");
	}
	
	// Update is called once per frame
	void Update () {

        if (!MenuScript.IsPaused)
        {
			if(Hero.GetComponent<LM_Shoot>().BaconCount==0){
            	transform.LookAt(Hero.transform.position);
            	transform.Translate(Vector3.forward * MoveSpeed * Time.deltaTime);
			}
			else{
				Vector3 baconPos = Hero.GetComponent<LM_Shoot>().Bacon_Position;
				transform.LookAt(baconPos);
				transform.Translate(Vector3.forward * MoveSpeed * Time.deltaTime);
			}
        }
	}
}
