﻿using UnityEngine;
using System.Collections;

public class BloodTracksScript : MonoBehaviour {

	// Use this for initialization
    public GameObject BloodSpawn1;
    public GameObject BloodSpawn2;
    public GameObject Bloodsplatter;

    public float BloodyTireDuration = 4;
    public float BloodSpawnInterval = 0.5f;
    private float _bloodyTireTimer = 0;
    private bool _bloodyTireEnabled = false;

	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (_bloodyTireEnabled)
        {
            _bloodyTireTimer += Time.deltaTime;
        }
        if (_bloodyTireTimer >= BloodyTireDuration && _bloodyTireEnabled)
        {
            _bloodyTireEnabled = false;
            _bloodyTireTimer = 0;
            CancelInvoke("SpawnBloodSplatter");
        }
	}

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Enemy")
        {
            _bloodyTireEnabled = true;
            InvokeRepeating("SpawnBloodSplatter", 0, BloodSpawnInterval);
        }

    }

    void SpawnBloodSplatter()
    {
        Instantiate(Bloodsplatter, new Vector3(BloodSpawn1.transform.position.x, 0, BloodSpawn1.transform.position.z), BloodSpawn1.transform.rotation);
        Instantiate(Bloodsplatter, new Vector3(BloodSpawn2.transform.position.x, 0, BloodSpawn1.transform.position.z), BloodSpawn2.transform.rotation);
    }

}
