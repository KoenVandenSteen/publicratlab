﻿using UnityEngine;
using System.Collections;

public class BaconScript : MonoBehaviour {

	public float BaconTime;
	//public GameObject Hero;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.transform.tag == "Enemy") {
			GameObject.Find("lawnmower").GetComponent<LM_Shoot>().BaconCount--;
			GameObject.Destroy(gameObject);		
		}
	
	}
}
