﻿using UnityEngine;
using System.Collections;

public class LM_Shoot : MonoBehaviour {

	public GameObject Bacon_Projectile;
	public Vector3 Bacon_Position;
	public int BaconCount;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetAxis("Space")>0 && BaconCount == 0){
			++BaconCount;
			Bacon_Position = transform.position;
			GameObject.Instantiate(Bacon_Projectile,transform.position,Quaternion.identity);
		}
	}
}
