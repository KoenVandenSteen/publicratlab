﻿using UnityEngine;
using System.Collections;

public class CheckpointScript : MonoBehaviour
{
    // Static
    private static int _lastNodePassed = 0;
    private static int _totalNodes = 0;
    private static System.Collections.Generic.List<CheckpointScript> _nodePositions = new System.Collections.Generic.List<CheckpointScript>();
    private static Quaternion _playerRotation;

    // Non-static
    private int _nodeID = _totalNodes - 1;
    private bool _initDone = false;
    private LevelScript _levelScript;

    private bool _animatedFlag = false;

	// Use this for initialization
	void Start () 
    {
        if (!_initDone)
        {
            _nodePositions.Add(this);
            _nodeID = _totalNodes;
            ++_totalNodes;
            name = "Checkpoint ID " + _nodeID;
            _levelScript = GameObject.Find("ScriptObject").GetComponent<LevelScript>();
            _initDone = true;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {

            if (!_initDone) Start();


            if (!_animatedFlag)
            {
                SetNodeLastPassed();
                LevelScript.savedProperties._player_flyTimeLeft = other.GetComponent<CharacterScript>().GetFlyTime();
                LevelScript.savedProperties._puzzlePieces = new System.Collections.Generic.List<int>(_levelScript.PuzzlePiecesAquired);
                //_levelScript.PuzzlePiecesAquired.Clear();

                if (GetComponent<AudioSource>())
                    GetComponent<AudioSource>().Play();
                _animatedFlag = true;
                var rotationScripts = GetComponentsInChildren<RotateDurationScript>();
                foreach (var item in rotationScripts)
                {
                    item.StartTimer();
                }
            }
        }
    }

    public static void ResetCheckpoints()
    {
        _nodePositions.Clear();
        _lastNodePassed = 0;
        _totalNodes = 0;
    }

    public void SetNodeLastPassed()
    {
        _lastNodePassed = _nodeID;
        _playerRotation = CharacterScript.GetControlTransform().rotation;
    }

    public static Transform GetLastNodePassedTransform() { return _nodePositions[_lastNodePassed].transform; }
    public static Quaternion GetLastNodePassedRotation() { return _playerRotation; }
}
