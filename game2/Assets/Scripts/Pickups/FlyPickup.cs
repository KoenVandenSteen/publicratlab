﻿using UnityEngine;
using System.Collections;

public class FlyPickup : PickUpTemplate 
{
    // Public variables
    public float PickUpPower = .5f;

    // Not used in this script
    //public override void StartPickup() { }
    //public override void UpdatePickup() { }

    public override void OnTriggerEnterPickup(Collider other)
    {
        _levelScript.PickUpsAquired++;
        _levelScript.UpdateScore();
        _player.GetComponent<CharacterScript>().AddFlyTime(PickUpPower);
        GetComponent<AudioSource>().Play();
    }
}
