﻿using UnityEngine;
using System.Collections;

// If you want to make a new pickup, take a look at the FlyPickup.cs script.

// Inherit from this class
public abstract class PickUpTemplate : MonoBehaviour 
{
    // Public variables
    public bool Active = true;
    public bool HideOnHit = true;

    // Private variables
    protected GameObject _player;
    protected LevelScript _levelScript;

    // Override these functions, using the override keyword
    public virtual void StartPickup() { }
    public virtual void UpdatePickup() { }
    public abstract void OnTriggerEnterPickup(Collider other);

	// Use this for initialization
	void Start() 
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _levelScript = GameObject.Find("ScriptObject").GetComponent<LevelScript>();
        StartPickup();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Active && !MenuScript.IsPaused)
        {
            // Do update here
            UpdatePickup();
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (Active && other.tag.Equals("Player")) 
        {
            if (HideOnHit)
            {
                LevelScript.savedProperties._pickups.Add(this);
                SetPickupActive(false);
            }

            OnTriggerEnterPickup(other);
        }
    }

    public void SetPickupActive(bool active)
    {
        Active = active;
        if (Active)
        {
            GetComponent<Collider>().enabled = true;
            GetComponent<Renderer>().enabled = true;
            var renderers = GetComponentsInChildren<Renderer>();
            foreach (var renderer in renderers)
            {
                renderer.enabled = true;
            }
        }
        else
        {
            GetComponent<Collider>().enabled = false;
            GetComponent<Renderer>().enabled = false;
            var renderers = GetComponentsInChildren<Renderer>();
            foreach (var renderer in renderers)
            {
                renderer.enabled = false;
            }
        }
    }
}
