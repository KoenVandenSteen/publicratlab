﻿using UnityEngine;
using System.Collections;

public class PuzzlePickup : PickUpTemplate {

    private static int _counter = 0;
    // Public variables
    //puzzle piece nr from 0 to -> nr of puzzle pieces in the level
    public int PuzzlePieceNr;
    public bool Override = false;
    // Not used in this script
    public override void StartPickup() { PuzzlePieceNr = Override? PuzzlePieceNr : _counter;  ++_counter; }
    //public override void UpdatePickup() { }

    public override void OnTriggerEnterPickup(Collider other)
    {
        _levelScript.UpdateScore();
        _levelScript.PickUpsAquired++;
        _levelScript.PuzzlePiecesAquired.Add(PuzzlePieceNr);
        LevelScript.savedProperties.NewCollectedPieces++;
        _levelScript.UpdateKeyUI();
        GetComponent<AudioSource>().Play();
    }

    public static void ResetCounter() {
        _counter = 0;
    }
}
