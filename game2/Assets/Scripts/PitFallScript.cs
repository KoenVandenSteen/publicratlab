﻿using UnityEngine;
using System.Collections;

public class PitFallScript : MonoBehaviour {

    public float DisableSpeed = 5;
    public float StartTime = 0;
    public float ActivationSpeed = 2;

    private MeshCollider _collider;
    private float _timer = 0;
    // Use this for initialization
    void Start()
    {
        _collider = gameObject.GetComponent<MeshCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (StartTime > 0) StartTime -= Time.deltaTime;
        if (StartTime <= 0 && !MenuScript.IsPaused)
        {
            _timer += Time.deltaTime;
            if (_collider.enabled)
            {
                if (_timer >= DisableSpeed / 2) ChangeAlpha(1 - (_timer - (DisableSpeed / 2)) / (DisableSpeed / 2));
                else ChangeAlpha(1.0f);

                if (_timer >= DisableSpeed) 
                {
                    _collider.enabled = false;
                    _timer = 0;
                }
            }
            else
            {
                if (_timer >= ActivationSpeed)
                {
                    _collider.enabled = true;
                    _timer = 0;
                }
            }

        }
    }

    void ChangeAlpha(float alpha)
    {
        Color oldColor = gameObject.GetComponent<Renderer>().material.color;
        Color newColor = new Color(oldColor.r, oldColor.g, oldColor.b, alpha);
        gameObject.GetComponent<Renderer>().material.SetColor("_Color", newColor);
    }
}
