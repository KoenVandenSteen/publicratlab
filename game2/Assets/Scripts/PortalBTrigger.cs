﻿using UnityEngine;
using System.Collections;

public class PortalBTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        transform.parent.GetComponent<Portals>().PortalBTrigger = true;
        GetComponent<AudioSource>().Play();
    }
}
