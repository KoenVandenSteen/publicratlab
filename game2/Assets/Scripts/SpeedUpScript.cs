﻿using UnityEngine;
using System.Collections;

public class SpeedUpScript : MonoBehaviour {

    public float SpeedUpPower = 1000;
    //choose wich direction the power upp will happen
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

        void OnTriggerEnter(Collider other){
            if (other.tag == "Player")
            {
                other.transform.GetComponent<Rigidbody>().AddTorque(-transform.right * SpeedUpPower, ForceMode.Impulse);
                other.transform.GetComponent<Rigidbody>().AddForce(-transform.right * SpeedUpPower, ForceMode.Impulse);
                GetComponent<AudioSource>().Play();
            }
        }

    //void OnTriggerExit(Collider other)
    //{
    //    if (other.tag == "Player")
    //    {
    //        other.transform.GetComponent<Rigidbody>().rotation = Quaternion.Euler(new Vector3(0, 0, 0));
    //        other.transform.GetComponent<Rigidbody>().velocity = new Vector3();
    //        Debug.Log("transform.forward: " + transform.forward + " total: " + (transform.forward * SpeedUpPower));
    //        other.transform.GetComponent<Rigidbody>().AddForce(transform.forward * SpeedUpPower, ForceMode.VelocityChange);
    //    }
    //}
}
