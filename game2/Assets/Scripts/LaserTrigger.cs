﻿using UnityEngine;
using System.Collections;

public class LaserTrigger : MonoBehaviour {

    public float LaserFlashSpeed = 5;

    public GameObject _laserBarrier;
    private BoxCollider _laserCollision;
    public float _currentFlashTime = 0;
    private bool _toggleLaser = false;
	// Use this for initialization
	void Start () {
        _laserCollision = gameObject.GetComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () {

        if (!MenuScript.IsPaused) {
            _currentFlashTime += Time.deltaTime;

            if (_currentFlashTime>=LaserFlashSpeed)
            {
                if (_toggleLaser)
                {
                    _toggleLaser = false;
                    _laserBarrier.SetActive(false);
                    _laserCollision.enabled = false;
                }
                else{
                    _toggleLaser = true;
                    _laserBarrier.SetActive(true);
                    _laserCollision.enabled = true;
                }
                _currentFlashTime = 0;
            }

        }
	}
}
