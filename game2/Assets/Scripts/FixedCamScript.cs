﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class FixedCamScript : MonoBehaviour 
{
    public Vector3 FixedCamPos;
        
    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            Camera.main.GetComponent<FollowCam>().SetCameraFixed(FixedCamPos, transform.position);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            Camera.main.GetComponent<FollowCam>().SetCameraFollow();
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(FixedCamScript))]
public class FixedCamScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        FixedCamScript myScript = target as FixedCamScript;

        GUI.changed = false;
        myScript.FixedCamPos = EditorGUILayout.Vector3Field("Camera position", myScript.FixedCamPos);
        if (GUILayout.Button("Set to viewport camera")) myScript.FixedCamPos = SceneView.lastActiveSceneView.camera.transform.position; 

        if (GUI.changed)
            EditorUtility.SetDirty(myScript);
    }
}

#endif