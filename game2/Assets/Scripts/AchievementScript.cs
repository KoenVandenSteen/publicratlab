﻿using UnityEngine;
using System.Collections;
//using UnityEngine.UI;

public abstract class AchievementScript : MonoBehaviour
{
    public enum TRACK_VALUES
    {
        SCORE_LEVEL,
        UNLOCK_LEVEL,
        DEATHS_TOTAL,
        TIME_LEVEL,
        TIME_PLAYED,
        PUZZLE_PIECES
    }

    public static int GAME_LEVELS = 12;

    public static int[] _scoreLevel = new int[GAME_LEVELS];
    private static int[] _puzzlePiecesLevel = new int[GAME_LEVELS];
    public static float[] _values = new float[6];

	public static void Initialize()
    {
        if (!PlayerPrefs.HasKey("DebugVersion")) PlayerPrefs.SetInt("DebugVersion", 0);
        if (PlayerPrefs.GetInt("DebugVersion") == 0) DebugVersion();

        if (!PlayerPrefs.HasKey(TRACK_VALUES.DEATHS_TOTAL.ToString())) PlayerPrefs.SetFloat(TRACK_VALUES.DEATHS_TOTAL.ToString(), 0);
        if (!PlayerPrefs.HasKey(TRACK_VALUES.TIME_PLAYED.ToString())) PlayerPrefs.SetFloat(TRACK_VALUES.TIME_PLAYED.ToString(), 0);
        if (!PlayerPrefs.HasKey(TRACK_VALUES.UNLOCK_LEVEL.ToString())) PlayerPrefs.SetFloat(TRACK_VALUES.UNLOCK_LEVEL.ToString(), 1);
        if (!PlayerPrefs.HasKey(TRACK_VALUES.SCORE_LEVEL.ToString())) PlayerPrefs.SetString(TRACK_VALUES.SCORE_LEVEL.ToString(), "");
        if (!PlayerPrefs.HasKey(TRACK_VALUES.PUZZLE_PIECES.ToString())) PlayerPrefs.SetString(TRACK_VALUES.PUZZLE_PIECES.ToString(), "");

        LoadAchievements();
	}

    public static void Update()
    {
        if (!MenuScript.IsPaused)
        {
            
        }
    }

    public static void DebugVersion()
    {
        PlayerPrefs.DeleteKey(TRACK_VALUES.SCORE_LEVEL.ToString());
        PlayerPrefs.SetInt("DebugVersion", 1);
    }

    public static void SetValue(TRACK_VALUES v, float val) { _values[(int)v] = val; }
    public static float GetValue(TRACK_VALUES v) { return _values[(int)v]; }
    public static bool GetPuzzlePieceCaptured(int level, int piece) { return ((_puzzlePiecesLevel[level - LevelScript.LevelMenuOffset] & (1 << piece)) > 0); }
    public static void SetPuzzlePieceCaptured(int level, int piece) { _puzzlePiecesLevel[level - LevelScript.LevelMenuOffset] |= (1 << piece); }

    public static void LoadAchievements()
    {
        _values[(int)TRACK_VALUES.UNLOCK_LEVEL] = PlayerPrefs.GetFloat(TRACK_VALUES.UNLOCK_LEVEL.ToString());
        _values[(int)TRACK_VALUES.DEATHS_TOTAL] = PlayerPrefs.GetFloat(TRACK_VALUES.DEATHS_TOTAL.ToString());
        _values[(int)TRACK_VALUES.TIME_PLAYED] = PlayerPrefs.GetFloat(TRACK_VALUES.TIME_PLAYED.ToString());

        // Load scores
        char[] Scores = PlayerPrefs.GetString(TRACK_VALUES.SCORE_LEVEL.ToString()).ToCharArray();
        for (int i = 0; i < Scores.Length; i += 8)
        {
            string score = ""; 
            for (int j = 0; j < 7; j++)
            {
                score += Scores[i + j];
                _scoreLevel[i / 8] = int.Parse(score);
            }
        }

        char[] Pieces = PlayerPrefs.GetString(TRACK_VALUES.PUZZLE_PIECES.ToString()).ToCharArray();
        for (int i = 0; i < Pieces.Length; i += 4)
        {
            string piecesStr = "";
            for (int j = 0; j < 3; j++)
            {
                piecesStr += Pieces[i + j];
                _puzzlePiecesLevel[i / 4] = int.Parse(piecesStr);
            }
        }
    }

    public static void SaveAllValues() 
    {
        string scores = "";
        for (int i = 0; i < _scoreLevel.Length; i++)
        {
            if (_scoreLevel[i] > 9999999) _scoreLevel[i] = 9999999;
            scores += _scoreLevel[i].ToString("0000000") + ",";
            //Debug.Log(scores);
        }
        PlayerPrefs.SetString(TRACK_VALUES.SCORE_LEVEL.ToString(), scores);

        //Puzzle pieces
        string puzzlePieces = "";
        for (int i = 0; i < _puzzlePiecesLevel.Length; i++)
        {
            if (_puzzlePiecesLevel[i] > 999) { Debug.Log("Sorry this should not be possible !"); _puzzlePiecesLevel[i] = 999; }
            puzzlePieces += _puzzlePiecesLevel[i].ToString("000") + ",";
        }
        PlayerPrefs.SetString(TRACK_VALUES.PUZZLE_PIECES.ToString(), puzzlePieces);

        PlayerPrefs.SetFloat(TRACK_VALUES.DEATHS_TOTAL.ToString(), _values[(int)TRACK_VALUES.DEATHS_TOTAL]);
        PlayerPrefs.SetFloat(TRACK_VALUES.TIME_PLAYED.ToString(), _values[(int)TRACK_VALUES.TIME_PLAYED]);
        PlayerPrefs.SetFloat(TRACK_VALUES.UNLOCK_LEVEL.ToString(), _values[(int)TRACK_VALUES.UNLOCK_LEVEL]);

        PlayerPrefs.Save(); 
    }
}
