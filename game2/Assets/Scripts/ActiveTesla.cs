﻿using UnityEngine;
using System.Collections;

public class ActiveTesla : MonoBehaviour {

    public float KnockBackForce = 100;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision collision){
        if (collision.gameObject.tag.Equals("Player"))
        {
            
           Vector3 force = transform.position-collision.transform.position;
           Debug.Log("bump: " + force);
           force.Normalize();
           collision.rigidbody.AddForce(-force * KnockBackForce, ForceMode.Impulse);
        }
    }
}
