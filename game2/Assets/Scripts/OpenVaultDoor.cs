﻿using UnityEngine;
using System.Collections;

public class OpenVaultDoor : MonoBehaviour {

    public bool OpenVault = false;
    public float OpenVaultSpeed = 40;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (OpenVault)
        {
            if (transform.GetChild(0).rotation.eulerAngles.y > 0)
            {
                transform.GetChild(0).Rotate(new Vector3(0, 1, 0) * Time.deltaTime * -OpenVaultSpeed);
            }
            else {
                OpenVault = false;
            }
        }
        else {
            if (transform.GetChild(0).rotation.eulerAngles.y < 180)
            {
                transform.GetChild(0).Rotate(new Vector3(0, 1, 0) * Time.deltaTime * OpenVaultSpeed);
            }
            else
            {
                OpenVault = true;
            }
        }
	}
}
