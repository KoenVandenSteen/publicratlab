﻿using UnityEngine;
using System.Collections;

public class PortalATrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other) {
        transform.parent.GetComponent<Portals>().PortalATrigger = true;
        GetComponent<AudioSource>().Play();
    }
}
