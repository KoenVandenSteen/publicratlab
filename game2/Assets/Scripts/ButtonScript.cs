﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    private GameObject _descriptiveText, _buttonText;
    public int Index = 0;

    // Use this for initialization
    void Start()
    {
        _descriptiveText = GameObject.Find(gameObject.name + "/LevelDescriptive");
        _buttonText = GameObject.Find(gameObject.name + "/TitleLevel");

        var buttonObject = gameObject.GetComponent<Button>();

        MenuScript.Initialize();
        int level;

        if (_buttonText.GetComponent<Text>().text.Contains("BONUS"))
        {
            level = Application.levelCount - 2;
            bool AllPiecesCollected = true;
            for (int i = LevelScript.LevelMenuOffset; i < Application.levelCount-1; i++)
            {
                for (int ii = 0; ii < 3; ii++)
                {
                    if (!AchievementScript.GetPuzzlePieceCaptured(i, ii))
                    {
                        AllPiecesCollected = false;
                        break;
                    }
                    if (!AllPiecesCollected) break;
                }
            }
            if (AllPiecesCollected)
            {
                buttonObject.interactable = true;
                _descriptiveText.GetComponent<Text>().text = "Best : " + AchievementScript._scoreLevel[level - LevelScript.LevelMenuOffset].ToString();
            }
            else _descriptiveText.GetComponent<Text>().text = "";
            buttonObject.onClick.AddListener(() => MenuScript.LoadLevelStatic(Application.levelCount - 1 ));
        }
        else
        {
            try
            {
                level = int.Parse(_buttonText.GetComponent<Text>().text.Remove(0, 6));
                int status = (int)AchievementScript.GetValue(AchievementScript.TRACK_VALUES.UNLOCK_LEVEL);

                if (level <= status)
                {
                    buttonObject.interactable = true;
                    _descriptiveText.GetComponent<Text>().text = "Best : " +
                        AchievementScript._scoreLevel[level - LevelScript.LevelMenuOffset].ToString();
                }
                else _descriptiveText.GetComponent<Text>().text = "";

                buttonObject.onClick.AddListener(() => MenuScript.LoadLevelStatic(level + LevelScript.LevelMenuOffset - 1));
            }
            catch (System.Exception)
            {
                Debug.Log("Something went wrong !");
            }
        }
    }
}
