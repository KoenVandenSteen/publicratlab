﻿using UnityEngine;
using System.Collections;

public class Portals : MonoBehaviour {

    public bool PortalATrigger = false;
    public bool PortalBTrigger = false;
    public GameObject Player;
	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (PortalATrigger && !PortalBTrigger) {
            Player.transform.position = transform.GetChild(1).transform.position;
            Player.GetComponent<Rigidbody>().velocity = Vector3.zero;
            Player.GetComponent<Rigidbody>().AddForce(transform.GetChild(1).transform.forward * 20f,ForceMode.Impulse);
        }

        if (PortalBTrigger && !PortalATrigger)
        {
            Player.transform.position = transform.GetChild(0).transform.position;
            Player.GetComponent<Rigidbody>().velocity = Vector3.zero;
            Player.GetComponent<Rigidbody>().AddForce(transform.GetChild(0).transform.forward * 20f, ForceMode.Impulse);
        }
	}


}
