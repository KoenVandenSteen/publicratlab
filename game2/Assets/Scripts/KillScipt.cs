﻿using UnityEngine;
using System.Collections;

public class KillScipt : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag.Equals("Player"))
            GameObject.Find("ScriptObject").GetComponent<LevelScript>().Respawn();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
            GameObject.Find("ScriptObject").GetComponent<LevelScript>().Respawn();
    }
}
