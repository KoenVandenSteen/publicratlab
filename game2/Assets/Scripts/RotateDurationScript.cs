﻿using UnityEngine;
using System.Collections;

public class RotateDurationScript : MonoBehaviour 
{
    // Public variables
    public float TimeToLive = 3;

    // Private variables
    private RotatingObjectScript _rotatingObjectScript;
    private float _timer = 0;

    // Public functions
    public void StartTimer()
    {
        _timer = TimeToLive;
        this.enabled = true;
        _rotatingObjectScript.enabled = true;
    }

	// Use this for initialization
	void Start () 
    {
        _rotatingObjectScript = GetComponent<RotatingObjectScript>();
        _rotatingObjectScript.enabled = false;
        this.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
    {
        _timer -= Time.deltaTime;
        if (_timer < 0)
        {
            _rotatingObjectScript.enabled = false;
            this.enabled = false;
        }
	}
}
