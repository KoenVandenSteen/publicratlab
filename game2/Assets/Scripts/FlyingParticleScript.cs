﻿using UnityEngine;
using System.Collections;

public class FlyingParticleScript : MonoBehaviour {

	// Use this for initialization
    private ParticleSystem _particles;
	void Start () 
    {
        _particles = gameObject.GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _particles.Play();
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            _particles.Stop();
        }
	
	}
}
