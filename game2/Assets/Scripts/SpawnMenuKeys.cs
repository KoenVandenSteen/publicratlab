﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpawnMenuKeys : MonoBehaviour
{
    public GameObject KeyElement;
    public Texture KeyCollected, KeyNotCollected;
    public Canvas currentCanvas;
    private float _zDepth = 2445;

    // Use this for initialization
    void Start()
    {
        DrawKeys();
    }

    void DrawKeys()
    {
        Vector3 _spawnLocation = new Vector3(0, 0, _zDepth);
        Vector2 _canvasSize = new Vector2(currentCanvas.GetComponent<RectTransform>().rect.width, currentCanvas.GetComponent<RectTransform>().rect.height);
        float _radius = _canvasSize.y / 2.0f - (_canvasSize.y/100f*2f);
        float _step = (2 * Mathf.PI) / (Application.levelCount - LevelScript.LevelMenuOffset -1);
        for (int i = 0; i < Application.levelCount - LevelScript.LevelMenuOffset - 1; i++)
        {
            _spawnLocation.x = (_canvasSize.x / 100) * 20 + _radius * Mathf.Cos(Mathf.PI / 2.0f + -i * _step);
            _spawnLocation.y = -_canvasSize.y / 2.0f + (_canvasSize.y/100f*10f) + _radius * Mathf.Sin(Mathf.PI / 2.0f + -i * _step);
            GameObject keyObject = Instantiate(KeyElement, _spawnLocation, Quaternion.Euler(0, 0, 0)) as GameObject;
            keyObject.GetComponentInChildren<Text>().text = "Level " + (i + 1);
            //UpdateKeys();

            keyObject.transform.SetParent(gameObject.transform, false);
        }

        //UpdateKeys();
    }

    public void UpdateKeys()
    {
        var keyObjects = GameObject.FindGameObjectsWithTag("BonusKeys");
        for (int i = 0; i < keyObjects.Length; i++)
        {
            var keyImages = keyObjects[i].GetComponentsInChildren<RawImage>();

            for (int ii = 0; ii < 3; ii++)
            {
                if (AchievementScript.GetPuzzlePieceCaptured(i + LevelScript.LevelMenuOffset, ii))
                {
                    keyImages[ii].canvasRenderer.SetAlpha(1f); 
                    keyImages[ii].texture = KeyCollected;
                }
                else
                {
                    keyImages[ii].canvasRenderer.SetAlpha(0.5f); 
                    keyImages[ii].texture = KeyNotCollected;
                }
            }
        }
    }
}
