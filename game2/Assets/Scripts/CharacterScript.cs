﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]

public class CharacterScript : MonoBehaviour
{
    // Public variables
    public float Speed = 8f;
    public float FlyingSpeed = 1f;
    public float FlyTime = 5f;
    public GameObject FlyBar;
    public float TurnSpeed = 60;
    public bool CollisionContinuous = true;
    public float FlyCamOffset = 5;
    public float MaxCameraTilt = 30;
    public float MaxFlySpeed = 50f;
    // Private variables
    private float _flyTimeLeft;
    private float _currentCameraTilt;
    private float _currentUpCameraTilt;
    private Transform _camera;
    private bool _updateState = false;
    private Vector3 _savedVel, _savedTorque;
    private bool _isFlying = false;
    private ParticleSystem _particles;
    private bool _isGrounded = false;

    // New camera and player turn control
    private static GameObject _playerControl; // Empty gameobject
    public static Transform GetControlTransform() { return _playerControl.transform; }
    public static void SetControlTransformRotation(Quaternion newRot) { _playerControl.transform.rotation = newRot; }

    // Respawn node
    private static Object _checkpointPrefab;

    // Use this for initialization
    void Start()
    {
        if (CollisionContinuous) GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        _playerControl = new GameObject();
        _playerControl.name = "CameraRotation control";

        if (!FlyBar) FlyBar = GameObject.Find("LevelUICanvas/FlyBar") as GameObject;
        _camera = Camera.main.transform;
        _camera.position = transform.position - transform.forward * 3 + transform.up;

        // Create new node to set start
        if (!_checkpointPrefab) _checkpointPrefab = Resources.Load("Prefabs/CheckpointPrefab");
        var SpawnNode = GameObject.Instantiate(_checkpointPrefab) as GameObject;
        SpawnNode.transform.position = transform.position;

        _playerControl.transform.rotation = transform.rotation;

        _particles = transform.GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        FlyBar.GetComponent<Scrollbar>().size = _flyTimeLeft / FlyTime;
        //Debug.Log(FlyBar.GetComponent<Scrollb.ar>().size);

        if (!MenuScript.IsPaused)
        {
            float horiz = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            float fly = Input.GetAxis("Jump");
            float turnCam = Input.GetAxis("TurnCamera");
            float turnCamVert = Input.GetAxis("TurnCameraVert");

            if (vertical > 0.1f)
            {
                if (!_isFlying)
                {
                    transform.GetComponent<Rigidbody>().AddTorque(_playerControl.transform.right * Speed * Time.deltaTime);
                }
                else 
                {
                    if (GetComponent<Rigidbody>().velocity.magnitude < MaxFlySpeed)
                        GetComponent<Rigidbody>().velocity += (_camera.up * FlyingSpeed * Time.deltaTime);
                    if (_currentUpCameraTilt > -MaxCameraTilt)
                    {
                        _currentUpCameraTilt += -Time.deltaTime * FlyingSpeed;
                        transform.Rotate(new Vector3(0, 1, 0), -Time.deltaTime * FlyingSpeed);
                    }
                }
            }
            else if (vertical < -0.1f)
            {
                if (!_isFlying)
                {
                    transform.GetComponent<Rigidbody>().AddTorque(_playerControl.transform.right * -Speed * Time.deltaTime);
                }
                else 
                {
                    if (GetComponent<Rigidbody>().velocity.magnitude < MaxFlySpeed)
                        GetComponent<Rigidbody>().velocity += (-_camera.up * FlyingSpeed * Time.deltaTime);
                    if (_currentUpCameraTilt > MaxCameraTilt)
                    {
                        _currentUpCameraTilt += Time.deltaTime * FlyingSpeed;
                        transform.Rotate(new Vector3(0, 1, 0), Time.deltaTime * FlyingSpeed);
                    }
                }
                
            }

            if (horiz > 0.2f)
            {
                if (_isFlying)
                {
                    if (GetComponent<Rigidbody>().velocity.magnitude < MaxFlySpeed)
                        GetComponent<Rigidbody>().velocity += (_camera.right * FlyingSpeed * Time.deltaTime);
                    if (_currentCameraTilt > -MaxCameraTilt)
                    {
                        _currentCameraTilt += -Time.deltaTime * FlyingSpeed;
                        transform.Rotate(new Vector3(0, 0, 1), -Time.deltaTime * FlyingSpeed);
                    }
                }
                else
                {
                    transform.GetComponent<Rigidbody>().AddTorque(_playerControl.transform.forward * -TurnSpeed * 5 * Time.deltaTime);
                }

            }
            else if (horiz < -0.2f)
            {
                if (_isFlying)
                {
                    
                    if (GetComponent<Rigidbody>().velocity.magnitude < MaxFlySpeed)
                        GetComponent<Rigidbody>().velocity += (-_camera.right * FlyingSpeed * Time.deltaTime);
                    if (_currentCameraTilt < MaxCameraTilt)
                    {
                        _currentCameraTilt += Time.deltaTime * FlyingSpeed;
                        transform.Rotate(new Vector3(0, 0, 1), Time.deltaTime * FlyingSpeed);
                    }
                }
                else
                {
                    transform.GetComponent<Rigidbody>().AddTorque(_playerControl.transform.forward * TurnSpeed * 5 * Time.deltaTime);
                }
            }
            else if (_isFlying)
            {
                _particles.Play();
                if (_currentCameraTilt < 1)
                {
                    _currentCameraTilt += FlyingSpeed * Time.deltaTime;
                    transform.Rotate(new Vector3(0, 0, 1), Time.deltaTime * FlyingSpeed);
                }
                else if (_currentCameraTilt > 1)
                {
                    _currentCameraTilt -= FlyingSpeed * Time.deltaTime;
                    transform.Rotate(new Vector3(0, 0, 1), -Time.deltaTime * FlyingSpeed);
                }

                if (_currentUpCameraTilt < 1)
                {
                    _currentUpCameraTilt += FlyingSpeed * Time.deltaTime;
                    transform.Rotate(new Vector3(0, 0, 1), Time.deltaTime * FlyingSpeed);
                }
                else if (_currentUpCameraTilt > 1)
                {
                    _currentUpCameraTilt -= FlyingSpeed * Time.deltaTime;
                    transform.Rotate(new Vector3(0, 0, 1), -Time.deltaTime * FlyingSpeed);
                }
            }

            if (transform.position.y < -20) GameObject.Find("ScriptObject").GetComponent<LevelScript>().Respawn();

            if (fly != 0 && _flyTimeLeft >= 0 && !_isGrounded )
            {
                _camera.GetComponent<FollowCam>().FlyCamEnable = true;
                _camera.position = transform.position + -_camera.forward * FlyCamOffset;
                GetComponent<Rigidbody>().angularVelocity = new Vector3();
                _flyTimeLeft -= Time.deltaTime;
                if (!_isFlying)
                {
                    _isFlying = true;
                    GetComponent<Rigidbody>().useGravity = false;
                    GetComponent<Rigidbody>().drag = 0.1f;
                    transform.rotation = Quaternion.Euler(_camera.rotation.eulerAngles + new Vector3(0,0,90));  
                }
            }
            else
            {
                _particles.Stop();
                _camera.GetComponent<FollowCam>().FlyCamEnable = false;
                _isFlying = false;
                gameObject.GetComponent<Rigidbody>().useGravity = true;
            }

            if (!FollowCam.IsMainCamFixed())
            {
                if (turnCam > 0.2f)
                    _playerControl.transform.Rotate(Vector3.up, TurnSpeed * Time.deltaTime);
                else if (turnCam < -0.2f)
                    _playerControl.transform.Rotate(Vector3.up, -TurnSpeed * Time.deltaTime);

                if (turnCamVert > 0.1f)
                    _camera.GetComponent<FollowCam>().IncementHeight(0.1f);
                else if (turnCamVert < -0.1f)
                    _camera.GetComponent<FollowCam>().IncementHeight(-0.1f);
            }
        }

        if (_updateState != MenuScript.IsPaused)
        {
            _updateState = MenuScript.IsPaused;
            if (_updateState)
            {
                _savedTorque = GetComponent<Rigidbody>().angularVelocity;
                _savedVel = GetComponent<Rigidbody>().velocity;
                GetComponent<Rigidbody>().Sleep();
            }
            else
            {
                GetComponent<Rigidbody>().WakeUp();
                GetComponent<Rigidbody>().velocity = _savedVel;
                GetComponent<Rigidbody>().angularVelocity = _savedTorque;
            }
        }
    }


    public void ResetRigidbody()
    {
        GetComponent<Rigidbody>().Sleep();
        GetComponent<Rigidbody>().velocity = new Vector3();
        GetComponent<Rigidbody>().angularVelocity = new Vector3();
        GetComponent<Rigidbody>().WakeUp();
    }

    public float GetFlyTime() { return _flyTimeLeft; }
    public void SetFlyTime(float newFlyTime) { _flyTimeLeft = newFlyTime; }

    public void AddFlyTime(float amountOfTime)
    {
        if (_flyTimeLeft < FlyTime)
        {
            _flyTimeLeft += amountOfTime;
            //_flyTimeLeft %= FlyTime;
            if (_flyTimeLeft > FlyTime) _flyTimeLeft = FlyTime;
        }
    }

    void OnCollisionEnter(Collision collision) {
        _isGrounded = true;
    }

    void OnCollisionExit(Collision collision)
    {
        _isGrounded = false;
    }
}
