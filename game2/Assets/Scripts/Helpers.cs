﻿using UnityEngine;
using System.Collections;
//using XInputDotNetPure;

public abstract class Helpers : MonoBehaviour 
{
    public static string ToMMSS(float time) 
    {
        float min = Mathf.Floor(time / 60);
        float sec = (time % 60);

        return min.ToString("00") + ":" + sec.ToString("00") + ":" + ((sec - Mathf.Floor(sec)) * 100).ToString("00");
    }
}

/*
public abstract class XInputController
{
    static bool playerIndexSet = false;
    static PlayerIndex playerIndex;
    static GamePadState state;
    static GamePadState prevState;

    // Update is called once per frame
    public static void Update()
    {
        // Find a PlayerIndex, for a single player game
        // Will find the first controller that is connected ans use it
        if (!playerIndexSet || !prevState.IsConnected)
        {
            for (int i = 0; i < 4; ++i)
            {
                PlayerIndex testPlayerIndex = (PlayerIndex)i;
                GamePadState testState = GamePad.GetState(testPlayerIndex);
                if (testState.IsConnected)
                {
                    playerIndex = testPlayerIndex;
                    playerIndexSet = true;
                }
            }
        }

        prevState = state;
        state = GamePad.GetState(playerIndex);
    }

    public static void SetVibration(int vibration)
    {
        // Set vibration according to triggers
        GamePad.SetVibration(playerIndex, vibration, vibration);
    }
}*/