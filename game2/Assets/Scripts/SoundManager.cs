﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	// Use this for initialization

    private static AudioSource _backgroundMusic;

	void Start () {
        if (_backgroundMusic == null) _backgroundMusic = GetComponent<AudioSource>();
        DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
        if (MenuScript.IsSplashDone()) {
            if (!_backgroundMusic.isPlaying)
                _backgroundMusic.Play();
        }
	}
}
