﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Base class for a scrolling select menu
/// </summary>
public class srcScrollerSelect : MonoBehaviour
{
    // Public datamembers
    public string ButtonTag = "tag";
    public float ScrollSpeed = 1.0f;
    public float ButtonGridSnap = 200.0f;
    public Vector3 MinScale = new Vector3(1.0f, 1.0f, 1.0f);
    public Vector3 MaxScale = new Vector3(2.0f, 2.0f, 2.0f);
    public MenuScrollerType ScrollerType = MenuScrollerType.GUI_Element;

    // Private datamembers
    public enum MenuScrollerType
    {
        GUI_Element, GameObject
    }

    protected struct MenuScrollerArray
    {
        public GameObject[] gameObjectArr;
        public MenuScrollerType scrollerType;
        public Vector3 minScale;
        public Vector3 maxScale;
        public float moveSpeed;

        public MenuScrollerArray(GameObject[] _objects, MenuScrollerType type, float moveSpeed, Vector3 minScale, Vector3 maxScale)
        {
            this.gameObjectArr = _objects;
            this.scrollerType = type;
            this.moveSpeed = moveSpeed;
            this.minScale = minScale;
            this.maxScale = maxScale;
        }
    }

    protected List<MenuScrollerArray> _menuObjects = new List<MenuScrollerArray>();
    protected float _buttonSelectAnim = 0;

    // Remembers the selected button even when changing scenes
    protected static List<int> _activeButton = new List<int>();
    protected static int _instances = 0;
    protected int _id = 0;
    protected int ActiveButton() { return _activeButton[_id]; }

    // Override these functions, using the override keyword
    protected virtual void StartScroller() { }
    protected virtual void FixedUpdateScroller() { }
    protected virtual void SnapButtonsScroller() { }
    protected virtual void ScrollLeftDone() { }
    protected virtual void ScrollRightDone() { }

    void Start()
    {
        _id = _instances++;
        _activeButton.Add(0);

        // Search for gameobject
        var buttonObjects = GameObject.FindGameObjectsWithTag(ButtonTag);

        // Add to list
        _menuObjects.Add(new MenuScrollerArray(buttonObjects, ScrollerType, ButtonGridSnap, MinScale, MaxScale));

        // Execute derived constructor
        StartScroller();

        // Sort by name -> TODO : make sure that buttons are named correctly
        foreach (var objectArray in _menuObjects)
            System.Array.Sort(objectArray.gameObjectArr, delegate(GameObject obj1, GameObject obj2)
            {
                return obj1.name.CompareTo(obj2.name);
            });

        // After sort, update selection
        foreach (var obj in _menuObjects)
        {
            switch (obj.scrollerType)
            {
                case MenuScrollerType.GUI_Element:
                    obj.gameObjectArr[ActiveButton()].GetComponent<RectTransform>().localScale = obj.maxScale;
                    foreach (var item in obj.gameObjectArr)
                        item.GetComponent<RectTransform>().localPosition -= new Vector3(obj.moveSpeed * ActiveButton(), 0, 0);
                    break;
                default:
                    obj.gameObjectArr[ActiveButton()].transform.localScale = obj.maxScale;
                    foreach (var item in obj.gameObjectArr)
                        item.transform.localPosition -= new Vector3(obj.moveSpeed * ActiveButton(), 0, 0);
                    break;
            }
        }
    }

    void FixedUpdate()
    {
        // Only execute when value has been set by 'public void DoScrollerMove(bool)'
        if (_buttonSelectAnim != 0)
        {
            // Move to the left
            if (_buttonSelectAnim < 0)
            {
                // Loop throught all arrays
                foreach (var objectArray in _menuObjects)
                {
                    for (int i = 0; i < objectArray.gameObjectArr.Length; i++)
                    {
                        // The set property of the struct defines if the object is a GUI object (on a canvas) or not
                        switch (objectArray.scrollerType)
                        {
                            case MenuScrollerType.GUI_Element:
                                objectArray.gameObjectArr[i].GetComponent<RectTransform>().localPosition -= new Vector3(objectArray.moveSpeed, 0, 0) * Time.fixedDeltaTime * ScrollSpeed;
                                if (i > 0)
                                    objectArray.gameObjectArr[ActiveButton() - 1].GetComponent<RectTransform>().localScale = Vector3.Lerp(objectArray.minScale, objectArray.maxScale, -_buttonSelectAnim);
                                if (ActiveButton() < objectArray.gameObjectArr.Length)
                                    objectArray.gameObjectArr[ActiveButton()].GetComponent<RectTransform>().localScale = Vector3.Lerp(objectArray.maxScale, objectArray.minScale, -_buttonSelectAnim);
                                break;
                            default:
                                objectArray.gameObjectArr[i].transform.localPosition -= new Vector3(objectArray.moveSpeed, 0, 0) * Time.fixedDeltaTime * ScrollSpeed;
                                if (i > 0)
                                    objectArray.gameObjectArr[ActiveButton() - 1].transform.localScale = Vector3.Lerp(objectArray.minScale, objectArray.maxScale, -_buttonSelectAnim);
                                if (ActiveButton() < objectArray.gameObjectArr.Length)
                                    objectArray.gameObjectArr[ActiveButton()].transform.localScale = Vector3.Lerp(objectArray.maxScale, objectArray.minScale, -_buttonSelectAnim);
                                break;
                        }
                    }
                }
                // Update time
                _buttonSelectAnim += Time.fixedDeltaTime * ScrollSpeed;
                if (_buttonSelectAnim > 0)
                {
                    _buttonSelectAnim = 0;
                    ScrollDone();
                    ScrollLeftDone();
                    SnapButtons();
                }
            }
            else
            {
                foreach (var objectArray in _menuObjects)
                {
                    for (int i = 0; i < objectArray.gameObjectArr.Length; i++)
                    {
                        switch (objectArray.scrollerType)
                        {
                            case MenuScrollerType.GUI_Element:
                                objectArray.gameObjectArr[i].GetComponent<RectTransform>().localPosition += new Vector3(objectArray.moveSpeed, 0, 0) * Time.fixedDeltaTime * ScrollSpeed;
                                if (ActiveButton() < objectArray.gameObjectArr.Length)
                                    objectArray.gameObjectArr[ActiveButton() + 1].GetComponent<RectTransform>().localScale = Vector3.Lerp(objectArray.minScale, objectArray.maxScale, _buttonSelectAnim);
                                if (i >= 0)
                                    objectArray.gameObjectArr[ActiveButton()].GetComponent<RectTransform>().localScale = Vector3.Lerp(objectArray.maxScale, objectArray.minScale, _buttonSelectAnim);
                                break;
                            default:
                                objectArray.gameObjectArr[i].transform.localPosition += new Vector3(objectArray.moveSpeed, 0, 0) * Time.fixedDeltaTime * ScrollSpeed;
                                if (ActiveButton() < objectArray.gameObjectArr.Length)
                                    objectArray.gameObjectArr[ActiveButton() + 1].transform.localScale = Vector3.Lerp(objectArray.minScale, objectArray.maxScale, _buttonSelectAnim);
                                if (i >= 0)
                                    objectArray.gameObjectArr[ActiveButton()].transform.localScale = Vector3.Lerp(objectArray.maxScale, objectArray.minScale, _buttonSelectAnim);
                                break;
                        }
                    }
                }

                _buttonSelectAnim -= Time.fixedDeltaTime * ScrollSpeed;
                if (_buttonSelectAnim < 0)
                {
                    _buttonSelectAnim = 0;
                    ScrollDone();
                    ScrollRightDone();
                    SnapButtons();
                }
            }
        }
        // Execute derived Update()
        FixedUpdateScroller();
    }

    private void ScrollDone()
    {
        foreach (var objectArray in _menuObjects)
            for (int i = 0; i < objectArray.gameObjectArr.Length; i++)
                switch (objectArray.scrollerType)
                {
                    case MenuScrollerType.GUI_Element:
                        objectArray.gameObjectArr[i].GetComponent<RectTransform>().localScale = (i == ActiveButton()) ? objectArray.maxScale : objectArray.minScale;
                        break;
                    default:
                        objectArray.gameObjectArr[i].transform.localScale = (i == ActiveButton()) ? objectArray.maxScale : objectArray.minScale;
                        break;
                }
    }

    private void SnapButtons()
    {
        // Loop through all elements and snap them to their move (grid)
        foreach (var objectArray in _menuObjects)
            foreach (var item in objectArray.gameObjectArr)
            {
                Vector3 V = (objectArray.scrollerType == MenuScrollerType.GUI_Element) ? item.GetComponent<RectTransform>().localPosition : item.transform.localPosition;
                V.x /= objectArray.moveSpeed;
                V.x = Mathf.Round(V.x);
                V.x *= objectArray.moveSpeed;
                if (objectArray.scrollerType == MenuScrollerType.GUI_Element) item.GetComponent<RectTransform>().localPosition = V;
                else item.transform.localPosition = V;
            }
        // Execute derived function
        SnapButtonsScroller();
    }

    public void DoScrollerMove(bool right)
    {
        if (_buttonSelectAnim == 0)
        {
            if (right && ActiveButton() > 0)
            {
                _buttonSelectAnim += 1;
                --_activeButton[_id];
            }
            else if (!right && ActiveButton() < _menuObjects[0].gameObjectArr.Length - 1)
            {
                _buttonSelectAnim -= 1;
                ++_activeButton[_id];
            }
        }
    }
}