﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DAEAnimScript : MonoBehaviour 
{
    private Texture[] _frames;
    private float _time = 0;
    private int _timeIndex = 0;
    private int _frameIndex = 0;
    private RawImage _destImage;

    private static bool _isDone = false;

	// Use this for initialization
	void Start () 
    {
        if (!_isDone)
        {
            Object[] a = Resources.LoadAll("Media/dae_anim");
            _frames = new Texture[a.Length];
            for (int i = 0; i < a.Length; i++) _frames[i] = a[i] as Texture;

            _destImage = GetComponent<RawImage>();
            _destImage.texture = _frames[_frameIndex];
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!_isDone)
        {
            _time += Time.deltaTime;
            if (_timeIndex == 0)
            {
                if (_time > 1)
                {
                    ++_timeIndex;
                    _time = 0;
                    _destImage.texture = _frames[++_frameIndex];
                }
            }
            else
            {
                if (_time > 0.03)
                {
                    _destImage.texture = _frames[++_frameIndex];
                    _time = 0;
                }
            }

            if (_frameIndex >= _frames.Length - 1) _isDone = true;
        }
	}
}
