﻿using UnityEngine;
using System.Collections;

public class FollowCam : MonoBehaviour 
{
    // Public variables
    public Transform Target;
    public float Damp = 0.8f;
    public float Distance = 3;
    public float Height = 1.5f;
    public float LerpSpeed = 10;
    public bool FlyCamEnable = false;
    private RaycastHit _hit;
    private float _prevHeight = 0;
    private float _camMoveCloser = 1;
    static private bool _isFixedCamera = false;
    private float _lerpToFixed;
    private Vector3 _lerpStartPos;
    private Vector3 _lerpEndPos;
    private Vector3 _lookAt;

    public void IncementHeight(float add) { Height += add; }
    
    void Start()
    {
        if (!Target) GameObject.FindGameObjectWithTag("Player");
        Damp = Mathf.Clamp(Damp, 0.01f, 10);
    }

    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.F1)) FreeCameraScript.SwitchCamera();

        if (FreeCameraScript.IsEnabled())
        {
            FreeCameraScript.Update();
        }
        else if (!MenuScript.IsPaused && !FlyCamEnable)
        {
            if (!_isFixedCamera)
	        {
	            Vector3 pTarget = Target.position;

                if (Physics.Raycast(transform.position - transform.forward, transform.forward, out _hit, Distance) 
                    && !_hit.collider.tag.Equals("Player") && !_hit.collider.tag.Equals("Checkpoint"))
                {
                    _camMoveCloser -= Time.deltaTime;
                    if (_camMoveCloser < 0.25f) _camMoveCloser = 0.25f;
                }
                else if (_camMoveCloser < 1) _camMoveCloser += Time.deltaTime;

                Vector3 newPos = pTarget - CharacterScript.GetControlTransform().forward * Distance * _camMoveCloser;
                newPos.y += (_prevHeight - pTarget.y) * Damp * Time.deltaTime + Height * _camMoveCloser;
                if (_lerpToFixed > 0)
                {
                    _lerpToFixed -= LerpSpeed * Time.deltaTime;
                    if (_lerpToFixed < 0) _lerpToFixed = 0;
                    transform.position = Vector3.Lerp(newPos, _lerpEndPos, _lerpToFixed);
                    transform.LookAt(Vector3.Lerp(pTarget, _lookAt, _lerpToFixed));
                }
                else
                {
                    transform.position = newPos; 
                    transform.LookAt(pTarget);
                }

                _prevHeight = pTarget.y; 
	        }
            else if (_lerpToFixed < 1)
            {
                _lerpToFixed += LerpSpeed * Time.deltaTime;
                if (_lerpToFixed > 1) _lerpToFixed = 1;
                transform.position = Vector3.Lerp(_lerpStartPos, _lerpEndPos, _lerpToFixed);
                transform.LookAt(Vector3.Lerp(Target.position, _lookAt, _lerpToFixed));
            }
        }
    }

    public static bool IsMainCamFixed() { return _isFixedCamera; }

    public void SetCameraFixed(Vector3 fixedCamPos, Vector3 lookAt)
    {
        _lerpStartPos = Target.position - CharacterScript.GetControlTransform().forward * Distance * _camMoveCloser;
        _lerpStartPos.y += (_prevHeight - Target.position.y) * Damp * Time.deltaTime + Height * _camMoveCloser;
        _lerpEndPos = fixedCamPos;
        _lookAt = lookAt;
        _isFixedCamera = true;
    }

    public void SetCameraFollow()
    {
        _lerpEndPos = transform.position;
        _isFixedCamera = false;
    }
}
