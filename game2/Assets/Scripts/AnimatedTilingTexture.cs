﻿using UnityEngine;
using System.Collections;

public class AnimatedTilingTexture : MonoBehaviour {

    public int colCount = 4;
    public int rowCount = 4;

    public int rowNumber = 0; 
    public int colNumber = 0; 
    public int totalCells = 4;
    public int fps = 10;

    private Vector2 offset;

    void Update() 
    { 
        SetSpriteAnimation(colCount, rowCount, rowNumber, colNumber, totalCells, fps); 
    }

    void SetSpriteAnimation(int colCount, int rowCount, int rowNumber, int colNumber, int totalCells, int fps)
    {

        int index = (int)(Time.time * fps);
        index = index % totalCells;

        float sizeX = 1.0f / colCount;
        float sizeY = 1.0f / rowCount;
        Vector2 size = new Vector2(sizeX, sizeY);

        var uIndex = index % colCount;
        var vIndex = index / colCount;

        float offsetX = (uIndex + colNumber) * size.x;
        float offsetY = (1.0f - size.y) - (vIndex + rowNumber) * size.y;
        Vector2 offset = new Vector2(offsetX, offsetY);

        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", offset);
        GetComponent<Renderer>().material.SetTextureScale("_MainTex", size);
    }
}
