﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelScript : MonoBehaviour
{
    public class SavedProperties
    {
        public void Reset() { _pickups.Clear(); NewCollectedPieces = 0; _player_flyTimeLeft = 0; }
        public float _player_flyTimeLeft;
        public System.Collections.Generic.List<PickUpTemplate> _pickups = new System.Collections.Generic.List<PickUpTemplate>();
        public System.Collections.Generic.List<int> _puzzlePieces = new System.Collections.Generic.List<int>();
        public int NewCollectedPieces = 0;
    }
    public static SavedProperties savedProperties = new SavedProperties();

    // Static variables
    //public static int Score = 0;
    public static int LevelMenuOffset = 1;

    // Public variables
    public float LevelTime = 120;
    public float CounterSpeed = 250;
    public float TimeScoreWeight = 10;
    public float PickUpScoreWeigth = 50;
    public float PickUpsAquired = 0;
    public System.Collections.Generic.List<int> PuzzlePiecesAquired = new System.Collections.Generic.List<int>();
    public int TwoStarScore = 800;
    public int ThreeStarScore = 1600;
    public Texture KeyCollected, KeyNotCollected;
    public bool FirstFinished = true;
    // Private variables
    private float _totalScore = 0;
    private float _timePassed = 0;
    private float _timeRemaining = 0;
    private Text _textTime, _textScore, _textTimeEnd, _textPickUp, _textTotalScore;
    private GameObject _KeysUI;
    private Canvas _pauseCanvas, _levelEndCanvas, _levelHUD;
    //private int _piecesCollected;
    private bool _waitingForButtonPress = false;
    private int _starsCollected = 0;
    //private static float _vibrate = 0;
    private static Texture _starTexture;

    // Use this for initialization
    void Start()
    {
        Camera.main.GetComponent<FollowCam>().SetCameraFollow();
        savedProperties.Reset();
        PickUpsAquired = 0;
        MenuScript.Initialize();
        _KeysUI = GameObject.Find("LevelUICanvas/UIKeyElements");
        _textScore = GameObject.Find("LevelUICanvas/TextScore").GetComponent<Text>();
        _textTime = GameObject.Find("LevelUICanvas/TextTime").GetComponent<Text>();
        _pauseCanvas = GameObject.Find("CanvasPause").GetComponent<Canvas>();
        _textTimeEnd = GameObject.Find("LevelEndCanvas/VAR_Time").GetComponent<Text>();
        _textPickUp = GameObject.Find("LevelEndCanvas/VAR_PickUps").GetComponent<Text>();
        _textTotalScore = GameObject.Find("LevelEndCanvas/VAR_TotalScore").GetComponent<Text>();
        _levelEndCanvas = GameObject.Find("LevelEndCanvas").GetComponent<Canvas>();
        _levelHUD = GameObject.Find("LevelUICanvas").GetComponent<Canvas>();
        if (!MenuScript.IsFinished) _levelEndCanvas.enabled = false;
        if (_pauseCanvas.enabled) _pauseCanvas.enabled = false;
        if (AchievementScript.GetValue(AchievementScript.TRACK_VALUES.UNLOCK_LEVEL) < Application.loadedLevel + LevelMenuOffset)
            AchievementScript.SetValue(AchievementScript.TRACK_VALUES.UNLOCK_LEVEL, Application.loadedLevel);

        if (!_starTexture) _starTexture = Resources.Load("Textures/star_fill") as Texture;
        //var button = GameObject.Find("CanvasPause/Button").GetComponent<Button>();
        //button.onClick.AddListener(() => MenuScript.LoadLevelStatic(0));

        var keyImages = _KeysUI.GetComponentsInChildren<RawImage>();

        for (int i = 0; i < 3; i++)
        {
            keyImages[i].canvasRenderer.SetAlpha(0.5f);
        }
        UpdateKeyUI();
    }

    // Update is called once per frame

    public void UpdateScore() {
                _totalScore += PickUpScoreWeigth;
    }

    public void UnPause()
    {
        MenuScript.IsPaused = false; 
        _pauseCanvas.enabled = false;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F2)) _levelHUD.enabled = !_levelHUD.enabled;
        if (!MenuScript.IsPaused && !MenuScript.IsFinished)
        {
            _timePassed += Time.deltaTime;
            _timeRemaining = LevelTime - _timePassed;
            _textTime.text = Helpers.ToMMSS(_timePassed);
           _textScore.text = "" + _totalScore;

            //if (_timePassed > LevelTime) { Respawn(); }

            if (Input.GetButtonDown("Pause")) { MenuScript.IsPaused = true; _pauseCanvas.enabled = true; }
        }
        else
        {
            if (Input.GetButtonDown("Pause")) { MenuScript.IsPaused = false; _pauseCanvas.enabled = false; }
        }

        if (MenuScript.IsFinished)
        {
            if (FirstFinished)
            {
                _totalScore = 0;

                //puts the puzzle pieces captured in the achievment script
                for (int i = 0; i < PuzzlePiecesAquired.Count; i++)
                {
                    AchievementScript.SetPuzzlePieceCaptured(Application.loadedLevel, PuzzlePiecesAquired[i]);
                }
                PuzzlePiecesAquired.Clear();
                FirstFinished = false;
            }

            _textTimeEnd.text = ((int)_timeRemaining).ToString() + " x " + TimeScoreWeight;
            _textPickUp.text = ((int)PickUpsAquired).ToString() + " x " + PickUpScoreWeigth;

            if (_timeRemaining > 0)
            {
                _timeRemaining -= Time.deltaTime * CounterSpeed;
                _totalScore += Time.deltaTime * CounterSpeed * TimeScoreWeight;
                if (_timeRemaining < 0) _timeRemaining = 0;
                CheckStars();
            }
            else if (PickUpsAquired > 0)
            {
                PickUpsAquired -= Time.deltaTime * CounterSpeed / 10f;
                _totalScore += Time.deltaTime * CounterSpeed / 10f * PickUpScoreWeigth;
                _textTotalScore.text = ((int)_totalScore).ToString();
                CheckStars();
                //if (PickUpsAquired <= 0)
                //{

                //}
            }
            else if (!_waitingForButtonPress)
            {
                if (AchievementScript._scoreLevel[Application.loadedLevel - LevelMenuOffset] < (int)_totalScore)
                    AchievementScript._scoreLevel[Application.loadedLevel - LevelMenuOffset] = (int)_totalScore;
                GameObject.Find("LevelEndCanvas/Continue").GetComponent<Button>().interactable = true;
                _waitingForButtonPress = true;
                AchievementScript.SetValue(AchievementScript.TRACK_VALUES.TIME_LEVEL, _timePassed);
            }
            else if (Input.GetButton("Submit")) MenuScript.LoadNextLevelStatic();
        }
    }

    private void CheckStars()
    {
        if (_starsCollected == 0 && _totalScore > 0)
        {
            _starsCollected = 1;
            GameObject.Find("LevelEndCanvas/RawImage_StarL").GetComponent<RawImage>().texture = _starTexture;
        }
        else if (_starsCollected == 1 && _totalScore > TwoStarScore)
        {
            _starsCollected = 2;
            GameObject.Find("LevelEndCanvas/RawImage_StarM").GetComponent<RawImage>().texture = _starTexture;
        }
        else if (_starsCollected == 2 && _totalScore > ThreeStarScore)
        {
            _starsCollected = 3;
            GameObject.Find("LevelEndCanvas/RawImage_StarR").GetComponent<RawImage>().texture = _starTexture;
        }
    }

    public void UpdateKeyUI() { 
            
            var keyImages = _KeysUI.GetComponentsInChildren<RawImage>();

            for (int i = 0; i < 3; i++)
            {
                keyImages[i].canvasRenderer.SetAlpha(0.5f);
                keyImages[i].texture = KeyNotCollected;
            }

            for (int i = 0; i < PuzzlePiecesAquired.Count; i++)
            {
               keyImages[PuzzlePiecesAquired[i]].canvasRenderer.SetAlpha(1f); 
               keyImages[PuzzlePiecesAquired[i]].texture = KeyCollected;
            } 
    }

    public void Respawn()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        var newTransform = CheckpointScript.GetLastNodePassedTransform();
        //newTransform.rotation = CheckpointScript.GetLastNodePassedRotation();
        var levelScript = GameObject.Find("ScriptObject").GetComponent<LevelScript>();
        player.transform.position = newTransform.position;
        CharacterScript.SetControlTransformRotation(CheckpointScript.GetLastNodePassedRotation());
        player.GetComponent<CharacterScript>().ResetRigidbody();
        
        player.GetComponent<CharacterScript>().SetFlyTime(savedProperties._player_flyTimeLeft);


        for (int i = 0; i < savedProperties._puzzlePieces.Count; i++)
            --savedProperties.NewCollectedPieces;

        levelScript.PuzzlePiecesAquired.Clear();
        levelScript.PuzzlePiecesAquired = new System.Collections.Generic.List<int>(savedProperties._puzzlePieces);

        UpdateKeyUI();

        for (int i = 0; i < savedProperties._pickups.Count; i++)
            savedProperties._pickups[i].SetPickupActive(true);


        savedProperties._pickups.Clear();
    }
}
