﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor; 
#endif

public class RotatingObjectScript : MonoBehaviour 
{
    public enum RotationPointEnum { Center, Pivot, Vector3, GameObject, Forward }

    // Public variables
    public float RotateSpeed = 10;
    public Vector3 RotationAxis = Vector3.up;
    public RotationPointEnum RotationPoint;
    public Vector3 _rotationPoint;

    // Do not touch
    public Object _rotPoint;

    // Private variables

	// Use this for initialization
	void Start () 
    {
        if (RotationPoint == RotationPointEnum.Center) _rotationPoint = GetComponent<Renderer>().bounds.center;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!MenuScript.IsPaused)
        {
            if (RotationPoint == RotationPointEnum.Forward) transform.Rotate(gameObject.transform.forward, RotateSpeed * Time.deltaTime);
            else if (RotationPoint == RotationPointEnum.Pivot) transform.Rotate(RotationAxis, RotateSpeed * Time.deltaTime);
            else transform.RotateAround(_rotationPoint, RotationAxis, RotateSpeed * Time.deltaTime);
        }
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(RotatingObjectScript))]
public class RotatingObjectScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        RotatingObjectScript myScript = target as RotatingObjectScript;

        myScript.RotateSpeed = EditorGUILayout.FloatField("Rotation Speed", myScript.RotateSpeed);
        myScript.RotationAxis = EditorGUILayout.Vector3Field("Rotation Axis", myScript.RotationAxis);
        myScript.RotationPoint = (RotatingObjectScript.RotationPointEnum)EditorGUILayout.EnumPopup("Rotation Point", myScript.RotationPoint);
        if (myScript.RotationPoint == RotatingObjectScript.RotationPointEnum.Vector3)
            myScript._rotationPoint = EditorGUILayout.Vector3Field("Position", myScript._rotationPoint);
        if (myScript.RotationPoint == RotatingObjectScript.RotationPointEnum.GameObject)
        {
            myScript._rotPoint = EditorGUILayout.ObjectField("Object Transform", myScript._rotPoint, typeof(Transform), true);
            if (myScript._rotPoint) myScript._rotationPoint = ((Transform)myScript._rotPoint).position;
        }

        if (GUI.changed)
            EditorUtility.SetDirty(myScript);
    }
}

#endif