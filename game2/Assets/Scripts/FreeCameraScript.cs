﻿using UnityEngine;
using System.Collections;

public abstract class FreeCameraScript
{
    private const float _turnSpeed = 0.1f;
    private const float _flySpeed = 0.5f;

    private static bool _isFreeCamEnabled = false;
    private static float _currSpeed = 0;
    private static Vector3 _mousePosition;
    private static Camera _freeCamera;

    public static void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) _currSpeed = _flySpeed * 2;
        else _currSpeed = _flySpeed;

        if (Input.GetMouseButton(0))
        {
            var mouseDiff = Input.mousePosition - _mousePosition;
            if (Mathf.Abs(mouseDiff.x) > 0.2f) _freeCamera.transform.Rotate(Vector3.up, mouseDiff.x * _turnSpeed);
            if (Mathf.Abs(mouseDiff.y) > 0.2f) _freeCamera.transform.Rotate(Vector3.right, mouseDiff.y * -_turnSpeed);
        }
        _mousePosition = Input.mousePosition;

        if (Input.GetKey(KeyCode.O)) _freeCamera.transform.Translate(Vector3.forward * _currSpeed);
        else if (Input.GetKey(KeyCode.L)) _freeCamera.transform.Translate(-Vector3.forward * _currSpeed);
        if (Input.GetKey(KeyCode.K)) _freeCamera.transform.Translate(-Vector3.right * _currSpeed);
        else if (Input.GetKey(KeyCode.M)) _freeCamera.transform.Translate(Vector3.right * _currSpeed);
    }

    public static bool IsEnabled() { return _isFreeCamEnabled; }
    public static void SwitchCamera()
    {
        _freeCamera = Camera.main;
        _isFreeCamEnabled = !_isFreeCamEnabled;
    }
}
