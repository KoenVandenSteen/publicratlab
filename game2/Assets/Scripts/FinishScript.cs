﻿using UnityEngine;
using System.Collections;

public class FinishScript : MonoBehaviour 
{
    private MenuScript _menuScript;

	// Use this for initialization
	void Start () 
    {
        _menuScript = GameObject.Find("ScriptObject").GetComponent<MenuScript>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnTriggerEnter(Collider other)
    {
        _menuScript.SwitchCanvas("CanvasLevelEnd");
        MenuScript.IsFinished = true;
        MenuScript.IsPaused = true;
        GameObject.Find("ScriptObject").GetComponent<LevelScript>().FirstFinished = true;
    }
}
