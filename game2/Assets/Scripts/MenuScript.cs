﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuScript : MonoBehaviour
{
    public Canvas Main, Controls, Level, Achievements, LevelEnd, BonusLevel;
    private string _oldCanvas = "CanvasMainMenu";
    public ColorBlock ButtonColor;
    public Texture KeyCollected, KeyNotCollected;
    // Menu button navigation controller
    private Button[] _buttons;
    private bool _released = true;
    private int _activeIndex = 0;

    public static void LoadLevelStatic(int i)
    {
        IsPaused = false;
        IsFinished = false;
        AchievementScript.SaveAllValues();
        Application.LoadLevel(i);
        CheckpointScript.ResetCheckpoints();
        PuzzlePickup.ResetCounter();
    }

    public static void LoadNextLevelStatic() 
    { 
        if(Application.loadedLevel > 7)
            LoadLevelStatic(0);
        else
            LoadLevelStatic(Application.loadedLevel + 1); 
    }
    public void LoadNextLevel() { LoadNextLevelStatic(); }
    public void LoadLevel(int i) { LoadLevelStatic(i); }
    public void ResetLevel() { LoadLevelStatic(Application.loadedLevel); }

    public void SetCanvasInvisible(string name) { SetCanvasVisibility(name, false); }
    public void SetCanvasVisibility(string name, bool visible)
    {
        switch (name)
        {
            case "CanvasMainMenu":
                if (Application.loadedLevelName == "menuscreen")
                {
                    Main.enabled = visible;
                    _buttons = Main.GetComponentsInChildren<Button>();
                    _activeIndex = 0;
                    //_buttons[_activeIndex].Select();
                }
                break;
            case "CanvasControls":
                if (Application.loadedLevelName == "menuscreen")
                {
                    Controls.enabled = visible;
                    _buttons = Controls.GetComponentsInChildren<Button>();
                    _activeIndex = 0;
                    //_buttons[_activeIndex].Select();
                }
                break;
            case "CanvasLevelSelect":
                if (Application.loadedLevelName == "menuscreen")
                {
                    Level.enabled = visible;
                    _buttons = Level.GetComponentsInChildren<Button>();
                    _activeIndex = 0;
                    UpdateKeyUI();
                    //_buttons[_activeIndex].Select();
                }
                break;
            case "CanvasAchievements":
                if (Application.loadedLevelName == "menuscreen")
                {
                    Achievements.enabled = visible;
                    _buttons = Achievements.GetComponentsInChildren<Button>();
                    _activeIndex = 0;
                    //_buttons[_activeIndex].Select();
                }
                break;
            case "CanvasBonusLevel":
                if (Application.loadedLevelName == "menuscreen")
                {
                    BonusLevel.enabled = visible;
                    _buttons = BonusLevel.GetComponentsInChildren<Button>();
                    _activeIndex = 0;
                    //_buttons[_activeIndex].Select();
                    BonusLevel.GetComponent<SpawnMenuKeys>().UpdateKeys();
                }
                break;
            case "CanvasLevelEnd": LevelEnd.enabled = visible; break;
        }

        if (Application.loadedLevel == 0)
        {
            _buttons[0].Select();
            foreach (var item in _buttons)
                item.GetComponent<Button>().colors = ButtonColor; 
        }
    }

    public void QuitApplication() { Application.Quit(); }
    public void UpdateKeyUI()
    {
        int index = 0;
        int buttonIndex = 0;
        foreach(var button in _buttons)
        {
            index = 0;
            foreach(var image in button.GetComponentsInChildren<RawImage>())
            {
                if(image.tag == "KeysLevelSelect"){
                    if(AchievementScript.GetPuzzlePieceCaptured(buttonIndex-1,index)){
                        image.texture = KeyCollected;
                        image.canvasRenderer.SetAlpha(1f); 
                    }
                    else
                    {
                        image.texture = KeyNotCollected;
                        image.canvasRenderer.SetAlpha(0.5f); 
                    }
                    index++;
                }
            }
            buttonIndex++;
        }
    }

    public void SwitchCanvas(string name)
    {
        SetCanvasVisibility(_oldCanvas, false);
        SetCanvasVisibility(name, true);
        _oldCanvas = name;
    }

    private static bool _initDone = false;
    public static bool Initialized() { return _initDone; }
    void Start()
    {
        Initialize();
        if (Application.loadedLevel == 0)
        {
            _buttons = Main.GetComponentsInChildren<Button>();
            if (_buttons.Length > _activeIndex) _buttons[_activeIndex].Select();
            for (int i = 0; i < _buttons.Length; i++)
                _buttons[i].colors = ButtonColor;
        }
        if (!_shownSplash) { SetCanvasInvisible(_oldCanvas); }
    }

    public static void Initialize()
    {
        if (!_initDone)
        {
            AchievementScript.Initialize();
            if (Application.loadedLevelName != "menuscreen") _shownSplash = true;
            _initDone = true;
        }
    }

    public static bool IsPaused;
    public static bool IsFinished;
    public void SetPaused(bool isPaused) { IsPaused = isPaused; }
    public void UnPause() { IsPaused = false; }

    // Code for splash screen
    private static bool _shownSplash = false; 
    private float _timerSplash = 0;
    private int _spashScreen = 0;
    private float _imageAlpha = 0;
    private bool _switchImage = true;
    private bool _makeVisible = true;
    private RawImage _currImage;

    public static bool IsSplashDone() { return _shownSplash; }
    void Update()
    {
        if (!_shownSplash)
        {
            if (_switchImage)
            {
                ++_spashScreen;
                switch (_spashScreen)
                {
                    case 1:
                        _currImage = GameObject.Find("CanvasSplash/RawImageDAE").GetComponent<RawImage>();
                        _currImage.enabled = true;
                        _switchImage = false;
                        break;
                    case 2:
                        _currImage.enabled = false;
                        _currImage = GameObject.Find("CanvasSplash/RawImageLogo").GetComponent<RawImage>();
                        _currImage.enabled = true;
                        _switchImage = false;
                        break;
                    default:
                        _currImage.enabled = false;
                        _switchImage = false;
                        _shownSplash = true;
                        SetCanvasVisibility(_oldCanvas, true);
                        break;
                }

                Color c = _currImage.color;
                c.a = 0;
                _currImage.color = c;
            }
            else
            {
                if (_makeVisible)
                {
                    _imageAlpha += Time.deltaTime;
                    if (_imageAlpha >= 1 && _timerSplash < 2)
                    {
                        _timerSplash += Time.deltaTime;
                        if (_timerSplash >= 2) { _makeVisible = false; _timerSplash = 0; }
                    }
                }
                else _imageAlpha -= Time.deltaTime;
                if (_imageAlpha < 0) { _imageAlpha = 0; _switchImage = true; _makeVisible = true; }
                if (_imageAlpha > 1) { _imageAlpha = 1; }
                Color c = _currImage.color;
                c.a = _imageAlpha;
                _currImage.color = c;
            }
        }
        else if (Application.loadedLevel == 0)
        {
            if (Input.GetButtonDown("Submit"))
            {
                var pointer = new PointerEventData(EventSystem.current);
                ExecuteEvents.Execute(_buttons[_activeIndex].gameObject, pointer, ExecuteEvents.pointerEnterHandler);
            }

            float move = Input.GetAxis("DPadVert") - Input.GetAxis("DPadHoriz");
            //move += (-Input.GetAxis("TurnCamera") * 5);
            if (_released)
            {
                if (move >= 1)
                {
                    --_activeIndex;
                    if (_activeIndex < 0) _activeIndex = _buttons.Length - 1;
                    _buttons[_activeIndex].Select();
                    _released = false;
                }
                else if (move <= -1)
                {
                    ++_activeIndex;
                    _activeIndex %= _buttons.Length;
                    _buttons[_activeIndex].Select();
                    _released = false;
                }
            }
            else if (move == 0) _released = true;
        }
    }

    private static int _konamiIndex = 0;
    private static KeyCode[] _konami = new KeyCode[]
    { 
        KeyCode.UpArrow, KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.DownArrow, 
        KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.LeftArrow, KeyCode.RightArrow, 
        KeyCode.B, KeyCode.A
    };

    void OnGUI()
    {
        var e = Event.current;
        if (e.isKey && Input.anyKeyDown && e.keyCode.ToString() != "None")
        {
            if (Input.GetKeyDown(_konami[_konamiIndex]))
            {
                ++_konamiIndex;
                if (_konamiIndex >= 10)
                {
                    var script = GameObject.Find("ScriptObject").GetComponent<LevelScript>();
                    if (script != null)
                    {
                        script.PickUpScoreWeigth = 1000;
                        script.TimeScoreWeight = 1000;
                        for (int i = 0; i < 8; i++)
                            script.PuzzlePiecesAquired.Add(i);
                        Debug.Log("KONAMI");
                    }
                }
            }
            else _konamiIndex = 0;
        }
    }
}