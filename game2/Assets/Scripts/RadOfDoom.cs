﻿using UnityEngine;
using System.Collections;

public class RadOfDoom : MonoBehaviour 
{
    public float RotationSpeed;
    public float OpenHatchPosition = 2;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!MenuScript.IsPaused)
        {
            transform.Rotate(new Vector3(1, 0, 0) * RotationSpeed * Time.deltaTime);

            for (int child = 0; child < transform.childCount; ++child)
            {
                transform.GetChild(child).Rotate(new Vector3(1, 0, 0) * -RotationSpeed * Time.deltaTime);
            } 
        }     
	}
}
