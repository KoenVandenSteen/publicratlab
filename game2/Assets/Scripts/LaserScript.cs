﻿using UnityEngine;
using System.Collections;

public class LaserScript : MonoBehaviour {

    public float BlinkTime = 10;
    public float TrappSpeed = 20;
    public float TotalOffset = 4;

    private float _currentOffset;
    private float _currentBlinkTime = 0;
    private bool _lightToggle = false;
    private bool _trapToggled = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        _currentBlinkTime += Time.deltaTime;

        if (_currentBlinkTime >= BlinkTime) {
            if (_lightToggle)
            {
                _lightToggle = false;
                transform.GetComponent<Light>().enabled = false;
            }
            else {
                _lightToggle = true;
                transform.GetComponent<Light>().enabled = true;
            }

            _currentBlinkTime = 0;  
        }

        if (_trapToggled)
        {
            if (_currentOffset <= TotalOffset)
                _currentOffset += Time.deltaTime * TrappSpeed;
                transform.FindChild("KnockUpbar").transform.Translate(transform.forward * -Time.deltaTime * TrappSpeed);
        }
	}

    void OnTriggerStay(Collider other) {
        if (_lightToggle && other.tag == "Player")
        {
            _trapToggled = true; 
        }
    }
}
