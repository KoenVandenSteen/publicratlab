﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MovingPlatform : MonoBehaviour 
{
    // Public variables
    public Vector3 StartPosition, EndPosition;
    public float LerpSpeed = 10;

    public float TimeOut = 0;

    // Private variables
    private float _lerpPos = 0;
    private bool _lerpIncrease = true;

    private bool _doTimeOut = false;
    private float _timeOut = 1;

	// Use this for initialization
	void Start ()
    {
        if (TimeOut > 0) _doTimeOut = true;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!MenuScript.IsPaused)
        {
            if (_timeOut <= 0)
            {
                transform.position = Vector3.Lerp(StartPosition, EndPosition, _lerpPos);
                if (_lerpIncrease)
                {
                    _lerpPos += Time.deltaTime * LerpSpeed;
                    if (_lerpPos > 1) 
                    {
                        _lerpPos = 1; _lerpIncrease = false;
                        transform.position = Vector3.Lerp(StartPosition, EndPosition, _lerpPos);
                        if (_doTimeOut) _timeOut = TimeOut; 
                    }
                }
                else
                {
                    _lerpPos -= Time.deltaTime * LerpSpeed;
                    if (_lerpPos < 0) 
                    {
                        _lerpPos = 0; _lerpIncrease = true;
                        transform.position = Vector3.Lerp(StartPosition, EndPosition, _lerpPos);
                        if (_doTimeOut) _timeOut = TimeOut;
                    }
                }
            }

            if (_doTimeOut) _timeOut -= Time.deltaTime;
        }
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(MovingPlatform))]
public class MovingPlatformEditor : Editor
{
    public override void OnInspectorGUI()
    {
        MovingPlatform myScript = target as MovingPlatform;

        myScript.LerpSpeed = EditorGUILayout.FloatField("Speed", myScript.LerpSpeed);
        myScript.TimeOut = EditorGUILayout.FloatField("Pause (seconds)", myScript.TimeOut);
        EditorGUILayout.BeginHorizontal();
        myScript.StartPosition = EditorGUILayout.Vector3Field("Start Position", myScript.StartPosition);
        if (GUILayout.Button("Set"))
            myScript.StartPosition = Selection.activeTransform.position;
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        myScript.EndPosition = EditorGUILayout.Vector3Field("End Position", myScript.EndPosition);
        if (GUILayout.Button("Set"))
            myScript.EndPosition = Selection.activeTransform.position;
        EditorGUILayout.EndHorizontal();
    }
}

#endif